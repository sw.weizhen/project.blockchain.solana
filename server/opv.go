package server

import (
	cr "gitlab.com/sw.weizhen/solana.kit/caller"

	rdbmsmysql "gitlab.com/sw.weizhen/db.mysql"
	"project.blockchain.solana/server/service"
)

var opvChannelRetry chan service.BlockRetry

var opvCFG *BootCfg

var OpvDB *rdbmsmysql.DBMySQL

var OpvC *cr.Caller
