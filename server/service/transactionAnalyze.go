package service

import (
	"encoding/json"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"

	ut "project.blockchain.solana/component/utils"
)

func fnTxAnalyze(blockTransaction interface{}) (*types.Transaction, error) {

	tx := new(types.Transaction)
	bytes, err := json.Marshal(blockTransaction)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, tx)
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func fnTxAssertFromOrTo(tx *types.Transaction) (string, []string, error) {

	accounts, err := tx.AccountMetaList()
	if err != nil {
		return "", []string{}, nil
	}

	frmAddr := ""
	toAddrs := []string{}
	for _, account := range accounts {
		if account.IsWritable && account.IsSigner {
			frmAddr = account.AKey.String()
		}

		if account.IsWritable && !account.IsSigner {
			toAddrs = append(toAddrs, account.AKey.String())
		}
	}

	return frmAddr, toAddrs, nil
}

func fnTxAssertDepositOrWithdraw(fromAddr, toAddr string) (int, error) {

	idxFrom, err := ut.ArrSearchWithLimit(fromAddr, DataAccounts, len(DataAccounts)/2)
	if err != nil {
		return 0, err
	}

	idxTo, err := ut.ArrSearchWithLimit(toAddr, DataAccounts, len(DataAccounts)/2)
	if err != nil {
		return 0, err
	}

	if idxFrom == -1 && idxTo == -1 {
		return 0, nil
	}

	txType := namespace.TxTypeDeposit
	if idxFrom >= 0 {
		txType = namespace.TxTypeWithdraw
	}

	if idxTo == idxFrom {
		txType = namespace.TxTypeSelfBalance
	}

	return txType, nil
}
