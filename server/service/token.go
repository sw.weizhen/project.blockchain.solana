package service

import (
	db "gitlab.com/sw.weizhen/db.mysql"
	"project.blockchain.solana/server/dao"
)

var DataTokens map[string]string

func ReloadToken(dbo *db.DBMySQL) (int, error) {

	dataLen := len(DataTokens)

	latestTokenCount, err := dao.QueryTokenCountDistinct(dbo)
	if err != nil {
		return 0, err
	}

	if latestTokenCount == dataLen {
		return 0, nil
	}

	diff := latestTokenCount - dataLen
	res, err := dao.QueryTokens(dbo)
	if err != nil {
		return diff, err
	}

	DataTokens = res

	return diff, nil
}
