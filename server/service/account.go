package service

import (
	db "gitlab.com/sw.weizhen/db.mysql"
	"project.blockchain.solana/server/dao"
)

var DataAccounts []string

func ReloadAccount(batch int, dbo *db.DBMySQL) (int, error) {

	dataLen := len(DataAccounts)

	latestAccountCount, err := dao.QueryAddressCount(dbo)
	if err != nil {
		return 0, err
	}

	if latestAccountCount == dataLen {
		return 0, nil
	}

	diff := latestAccountCount - dataLen
	res, err := dao.QueryAccountBatchWithLimit(batch, dbo)
	if err != nil {
		return diff, err
	}

	DataAccounts = make([]string, 0, len(res))
	DataAccounts = append(DataAccounts, res...)

	return diff, nil
}
