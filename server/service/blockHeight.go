package service

import (
	"context"
	"time"

	db "gitlab.com/sw.weizhen/db.mysql"
	cr "gitlab.com/sw.weizhen/solana.kit/caller"
	"gitlab.com/sw.weizhen/solana.kit/namespace"
	zl "project.blockchain.solana/component/zerolog"
	"project.blockchain.solana/server/dao"
)

type BlockRetry struct {
	Slot  uint64
	Count int
}

func AssertBlockHeight(rpcTimeout int, caller *cr.Caller, dbo *db.DBMySQL) error {

	ctx, bhCancel := context.WithTimeout(context.TODO(), time.Second*time.Duration(rpcTimeout))
	defer bhCancel()

	bhCnt, err := dao.QueryBlockHeightCount(dbo)
	if err != nil {
		return err
	}

	if bhCnt == 0 {
		out, err := caller.GetLatestBlockhash(ctx, namespace.COMMITMENT_FINALIZED)
		if err != nil {
			return err
		}

		_, err = dao.InsertBlockHeight(int64(out.SolContext.Context.Slot), dbo)
		if err != nil {
			return err
		}
	}

	return nil
}

func AssertBlockRNG(limit uint64, rpcTimeoutSec int, caller *cr.Caller, dbo *db.DBMySQL) (head uint64, tail uint64, slot uint64, diff uint64, id int, pass bool) {

	id, currHeight, err := dao.QueryBlockHeight(dbo)
	if err != nil {
		zl.L.Error().Uint64("limit", limit).Int("rpcTimeoutSec", rpcTimeoutSec).Msgf("AssertBlockRNG::QueryBlockHeight() -> %v", err)
		return
	}

	head = currHeight + 1
	tail = head + (limit - 1)

	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*time.Duration(rpcTimeoutSec))
	defer cancel()

	out, err := caller.GetLatestBlockhash(ctx, namespace.COMMITMENT_FINALIZED)
	if err != nil {
		zl.L.Error().Uint64("limit", limit).Int("rpcTimeoutSec", rpcTimeoutSec).Msgf("AssertBlockRNG::GetLatestBlockhash() -> %v", err)

		<-time.After(time.Millisecond * 3000)

		return
	}

	// out = nil
	slot = out.SolContext.Context.Slot
	if slot <= currHeight {
		// zl.L.Debug().Uint64("limit", limit).Int("rpcTimeoutSec", rpcTimeoutSec).Msgf("AssertBlockRNG() slot %d less than current %d", slot, currHeight)

		return
	}

	if tail > slot {
		tail = slot
	}

	diff = slot - tail
	pass = true

	return
}
