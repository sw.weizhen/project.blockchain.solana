package service

import (
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
	"project.blockchain.solana/server/dao"

	zl "project.blockchain.solana/component/zerolog"
)

func handleTransactionSolana(signature string, idxTX int, slot uint64, blockTime int64, txObj *types.Transaction, blockTX types.BlockTransaction) ([]dao.Transaction, bool) {

	daoTransactions := []dao.Transaction{}

	var amount decimal.Decimal
	var fee decimal.Decimal
	var tokenType string
	var fmAddr string
	var toAddr string

	fromAddr, toAddrs, err := fnTxAssertFromOrTo(txObj)
	if err != nil {

		zl.L.Error().Str("signature", signature).Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleTransactionSolana::fnTxAssertFromOrTo() -> %v", err)

		return daoTransactions, false
	}

	fmAddr = fromAddr

	if len(blockTX.Meta.PostBalances) < 2 || len(blockTX.Meta.PreBalances) < 2 || len(txObj.Message.MsgAccountKeys) < 2 || len(txObj.Signatures) < 1 {

		return daoTransactions, false
	}

	tmpAmount := int64(0)
	toAddressAmounts := []types.AddressAmount{}

	for _, addr := range toAddrs {

		amountIdx := -1
		for i, v := range txObj.Message.MsgAccountKeys {
			if v.String() == addr {
				amountIdx = i

				break
			}
		}

		if amountIdx == -1 {
			return daoTransactions, false
		}

		tmpAmount = blockTX.Meta.PostBalances[amountIdx] - blockTX.Meta.PreBalances[amountIdx]
		if tmpAmount > 0 {
			toAddr = addr
			amount = types.ConvertSolBalanceToDecimal(uint64(tmpAmount))

			if amount.Cmp(decimal.Zero) <= 0 {
				continue
			}

			toAddressAmounts = append(toAddressAmounts, types.AddressAmount{
				Address: toAddr,
				Amount:  amount,
			})
		}
	}

	if len(toAddressAmounts) < 1 {
		return daoTransactions, false
	}

	tmNow := time.Now().Unix()
	fee = types.ConvertSolBalanceToDecimal(blockTX.Meta.Fee)

	for _, toADDR := range toAddressAmounts {

		txType, err := fnTxAssertDepositOrWithdraw(fmAddr, toADDR.Address)
		if err != nil {
			zl.L.Error().Str("signature", signature).Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleTransactionSolana::fnTxAssertDepositOrWithdraw(%s, %s) -> %v", fmAddr, toADDR.Address, err)

			continue
		}

		if txType == 0 {
			continue
		}

		tokenType = namespace.CryptoType

		daoTransactions = append(daoTransactions, dao.Transaction{
			BlockHeight:      slot,
			TxType:           txType,
			TransactionTime:  blockTime,
			TxHash:           signature,
			CryptoType:       tokenType,
			FromAddress:      fmAddr,
			ToAddress:        toADDR.Address,
			Amount:           toADDR.Amount,
			TransactionIndex: idxTX,
			Fee:              fee,
			FeeCrypto:        namespace.CryptoType,
			ChainType:        namespace.ChainType,
			Status:           namespace.TxStatusWaitConfirm,
			NotifyStatus:     namespace.TxNotifyStatusNotYetProcessed,
			CreateTime:       tmNow,
			UpdateTime:       tmNow,
		})
	}

	return daoTransactions, true
}
