package service

import (
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
	"project.blockchain.solana/server/dao"

	zl "project.blockchain.solana/component/zerolog"
)

func handleTransactionToken(signature string, idxTX int, slot uint64, blockTime int64, blockTX types.BlockTransaction) ([]dao.Transaction, bool) {

	daoTransactions := []dao.Transaction{}

	var amount decimal.Decimal
	var fee decimal.Decimal
	var tokenType string
	var fmAddr string
	var toAddr string

	preTokenBalances := blockTX.Meta.PreTokenBalances
	postTokenBalances := blockTX.Meta.PostTokenBalances

	if len(postTokenBalances) < 2 {
		return daoTransactions, false
	}

	contractAddr := postTokenBalances[0].Mint
	tokenType = DataTokens[contractAddr]

	fmAddr = types.PublicKeyFromString(postTokenBalances[1].Owner).String()
	toAddr = types.PublicKeyFromString(postTokenBalances[0].Owner).String()
	fee = types.ConvertSolBalanceToDecimal(blockTX.Meta.Fee)

	if len(preTokenBalances) == 2 {

		amount = types.ConvertStrBalanceToDecimal(postTokenBalances[0].UITokenAmount.Amount, int(postTokenBalances[0].UITokenAmount.Decimals)).Sub(types.ConvertStrBalanceToDecimal(preTokenBalances[0].UITokenAmount.Amount, int(preTokenBalances[0].UITokenAmount.Decimals)))
		if !amount.IsPositive() {
			amount = types.ConvertStrBalanceToDecimal(postTokenBalances[1].UITokenAmount.Amount, int(postTokenBalances[1].UITokenAmount.Decimals)).Sub(types.ConvertStrBalanceToDecimal(preTokenBalances[1].UITokenAmount.Amount, int(preTokenBalances[1].UITokenAmount.Decimals)))
			fmAddr = postTokenBalances[0].Owner
			toAddr = postTokenBalances[1].Owner
		}

	} else if len(preTokenBalances) == 1 {

		amount = types.ConvertStrBalanceToDecimal(postTokenBalances[0].UITokenAmount.Amount, int(postTokenBalances[0].UITokenAmount.Decimals))
		fmAddr = preTokenBalances[0].Owner

		for _, postTokenBalance := range postTokenBalances {
			if postTokenBalance.Owner != fmAddr {
				toAddr = postTokenBalance.Owner
				amount = types.ConvertStrBalanceToDecimal(postTokenBalance.UITokenAmount.Amount, int(postTokenBalance.UITokenAmount.Decimals))
			}
		}

	} else if len(preTokenBalances) == len(postTokenBalances) {

		postLen := len(postTokenBalances)
		amount = types.ConvertStrBalanceToDecimal(postTokenBalances[postLen-1].UITokenAmount.Amount, int(postTokenBalances[postLen-1].UITokenAmount.Decimals)).Sub(types.ConvertStrBalanceToDecimal(preTokenBalances[postLen-1].UITokenAmount.Amount, int(preTokenBalances[1].UITokenAmount.Decimals)))
		toAddr = postTokenBalances[postLen-1].Owner
		fmAddr = postTokenBalances[0].Owner
	}

	txType, err := fnTxAssertDepositOrWithdraw(fmAddr, toAddr)
	if err != nil {
		zl.L.Error().Str("signature", signature).Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleTransactionToken::fnTxAssertDepositOrWithdraw(%s, %s) -> %v", fmAddr, toAddr, err)

		return daoTransactions, false
	}

	if txType == 0 {
		return daoTransactions, false
	}

	tmNow := time.Now().Unix()

	daoTransaction := dao.Transaction{
		BlockHeight:      slot,
		TxType:           txType,
		TransactionTime:  blockTime,
		TxHash:           signature,
		CryptoType:       tokenType,
		ContractAddr:     contractAddr,
		FromAddress:      fmAddr,
		ToAddress:        toAddr,
		Amount:           amount,
		TransactionIndex: idxTX,
		Fee:              fee,
		FeeCrypto:        namespace.CryptoType,
		ChainType:        namespace.ChainType,
		Status:           namespace.TxStatusWaitConfirm,
		NotifyStatus:     namespace.TxNotifyStatusNotYetProcessed,
		CreateTime:       tmNow,
		UpdateTime:       tmNow,
	}

	daoTransactions = append(daoTransactions, daoTransaction) // here is ok

	// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ why do this(copy from cryp-sol) ???? ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	// var fmAddrIdx int
	// accounts, err := txObj.AccountMetaList()
	// if err != nil {
	// 	zl.L.Error().Str("signature", signature).Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleTransactionToken::AccountMetaList() -> %v", err)

	// 	return daoTransactions, true
	// }

	// for idx, account := range accounts {
	// 	findAddr, _ := utils.ArrSearchWithLimit(account.AKey.String(), DataAccounts, len(DataAccounts)/2)
	// 	if findAddr != -1 && account.IsSigner {
	// 		fmAddrIdx = idx
	// 		break
	// 	}
	// }

	// isOwner, _ := utils.ArrSearchWithLimit(fmAddr, DataAccounts, len(DataAccounts)/2)
	// changeSOLAmount := types.ConvertSolBalanceToDecimal(uint64(blockTX.Meta.PreBalances[fmAddrIdx] - blockTX.Meta.PostBalances[fmAddrIdx])).Sub(fee)

	// if isOwner != -1 && changeSOLAmount.Cmp(decimal.Zero) != 0 {

	// 	txType, err = fnTxAssertDepositOrWithdraw(fmAddr, toAddr)
	// 	if err != nil {
	// 		zl.L.Error().Str("signature", signature).Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleTransactionToken::fnTxAssertDepositOrWithdraw(%s, %s) -> %v", fmAddr, toAddr, err)

	// 		return daoTransactions, true
	// 	}

	// 	toTokenAccount, _, err := types.FindAssociatedTokenAddress(types.PublicKeyFromString(toAddr), types.PublicKeyFromString(contractAddr))
	// 	if err != nil {
	// 		zl.L.Error().Str("signature", signature).Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleTransactionToken::FindAssociatedTokenAddress(%s, %s) -> %v", toAddr, contractAddr, err)

	// 		return daoTransactions, true
	// 	}

	// 	toAddr = toTokenAccount.String()

	// 	daoTransaction := dao.Transaction{
	// 		BlockHeight:      slot,
	// 		TxType:           txType,
	// 		TransactionTime:  blockTime,
	// 		TxHash:           signature,
	// 		CryptoType:       tokenType,
	// 		ContractAddr:     contractAddr,
	// 		FromAddress:      fmAddr,
	// 		ToAddress:        toAddr,
	// 		Amount:           amount,
	// 		TransactionIndex: idxTX,
	// 		GasPrice:         fee,
	// 		Fee:              fee,
	// 		FeeCrypto:        namespace.CryptoType,
	// 		ChainType:        namespace.ChainType,
	// 		Status:           namespace.TxStatusWaitConfirm,
	// 		NotifyStatus:     namespace.TxNotifyStatusNotYetProcessed,
	// 		CreateTime:       tmNow,
	// 		UpdateTime:       tmNow,
	// 	}

	// 	daoTransactions = append(daoTransactions, daoTransaction)
	// }
	// ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ why do this(copy from cryp-sol) ???? ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

	return daoTransactions, true
}
