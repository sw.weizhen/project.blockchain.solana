package service

import (
	"context"

	cr "gitlab.com/sw.weizhen/solana.kit/caller"
	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"

	ut "project.blockchain.solana/component/utils"
)

func GetLatestBlockhash(ctx context.Context, caller *cr.Caller) (uint64, error) {

	out, err := caller.GetLatestBlockhash(ctx, namespace.COMMITMENT_FINALIZED)
	if err != nil {
		return 0, err
	}

	return out.SolContext.Context.Slot, nil
}

func GetBlock(ctx context.Context, slot uint64, c *cr.Caller) (*types.Block, error) {

	respGetBlock, err := c.GetBlock(ctx, slot, &types.BlockConfigurationObject{
		Commitment:                     namespace.COMMITMENT_FINALIZED,
		Encoding:                       namespace.ENCODING_JSON, // only support encoding JSON
		TransactionDetails:             namespace.TRANSACTION_DETAILS_FULL,
		MaxSupportedTransactionVersion: ut.PtrNew[uint64](0),
		Rewards:                        ut.PtrNew(true),
	})

	if err != nil {
		return nil, err
	}

	return respGetBlock, nil
}
