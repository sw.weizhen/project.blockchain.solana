package service

import (
	db "gitlab.com/sw.weizhen/db.mysql"
	"gitlab.com/sw.weizhen/solana.kit/types"
	zl "project.blockchain.solana/component/zerolog"
	"project.blockchain.solana/server/dao"
)

func HandleBlockTransactions(blockTXs []types.BlockTransaction, slot uint64, blockTime int64, dbo *db.DBMySQL) (uint64, error) {

	daoTXs := []dao.Transaction{}

	for i := 0; i < len(blockTXs); i++ {

		tx, ok := handleBlockTX(i, blockTXs[i], slot, blockTime, dbo)
		if ok {
			daoTXs = append(daoTXs, tx...)
		}
	}

	err := dao.TXUpdWithdrawAndInsTransaction(daoTXs, dbo)
	if err != nil {

		zl.L.Error().Uint64("slot", slot).Msgf("HandleBlockTransactions::TXUpdWithdrawAndInsTransaction() -> %v", err)

		return slot, err
	}

	return slot, nil
}

func handleBlockTX(idxTX int, blockTX types.BlockTransaction, slot uint64, blockTime int64, dbo *db.DBMySQL) ([]dao.Transaction, bool) {

	var daoTransactions []dao.Transaction

	if blockTX.Meta.Err != nil {

		return daoTransactions, false
	}

	txObj, err := fnTxAnalyze(blockTX.Transaction)
	if err != nil {

		zl.L.Error().Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleBlockTX::fnTxAnalyze() -> %v", err)

		return daoTransactions, false
	}

	if len(txObj.Signatures) < 1 {

		return daoTransactions, false
	}

	var signature string = txObj.Signatures[0].String()

	dbTxCount, err := dao.QueryTransactionCount(signature, dbo)
	if err != nil {

		zl.L.Error().Int("idxTX", idxTX).Uint64("slot", slot).Msgf("handleBlockTX::QueryTransactionCount(%s) -> %v", signature, err)

		return daoTransactions, false
	}

	if dbTxCount > 0 {
		return daoTransactions, false
	}

	if len(blockTX.Meta.PostTokenBalances) == 0 {

		daoTXs, ok := handleTransactionSolana(signature, idxTX, slot, blockTime, txObj, blockTX)
		if !ok {
			return daoTransactions, false
		}

		daoTransactions = append(daoTransactions, daoTXs...)

	} else {

		daoTXs, ok := handleTransactionToken(signature, idxTX, slot, blockTime, blockTX)
		if !ok {
			return daoTransactions, false
		}

		daoTransactions = append(daoTransactions, daoTXs...)
	}

	return daoTransactions, true
}
