package main

import (
	"fmt"

	"project.blockchain.solana/server"
	// "project.blockchain.solana/server/dao"
)

func main() {
	fmt.Println("server::exam::main() -> entry")

	server.Boot()

	// err := dao.TXUpdWithdrawAndInsTransaction(txs, server.OpvDB)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
}
