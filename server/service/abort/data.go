package main

import "project.blockchain.solana/server/dao"

func init() {

	txs = []dao.Transaction{
		{TxHash: "tx_hash_1", TxType: 1},
		{TxHash: "tx_hash_2", TxType: 1},
		{TxHash: "tx_hash_3", TxType: 2},
		{TxHash: "tx_hash_4", TxType: 2},
		{TxHash: "tx_hash_5", TxType: 2},
		{TxHash: "tx_hash_6", TxType: 2},
		{TxHash: "tx_hash_7", TxType: 1},
		{TxHash: "tx_hash_8", TxType: 1},
		{TxHash: "tx_hash_9", TxType: 1},
	}
}

var txs []dao.Transaction
