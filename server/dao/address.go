package dao

import (
	"fmt"
	"strings"

	db "gitlab.com/sw.weizhen/db.mysql"
	ut "project.blockchain.solana/component/utils"
)

func batchAccountByID(batch int, syntax string, dbo *db.DBMySQL) ([]string, error) {

	var res []string

	conn, err := dbo.Manual()
	if err != nil {
		return res, err
	}

	stmt, err := conn.Prepare(syntax)
	if err != nil {
		return res, err
	}

	head := 1
	tail := batch

	for {
		tmp := make([]string, 0, batch)

		rows, err := stmt.Query(head, tail)
		if err != nil {
			return res, err
		}

		for rows.Next() {
			var col string

			err := rows.Scan(&col)
			if err != nil {
				return []string{}, err
			}

			tmp = append(tmp, strings.TrimSpace(col))
		}
		rows.Close()

		res = append(res, tmp...)

		if len(tmp) < batch {

			ut.ArrPDQSort[string](res, 0, len(res),
				func(x, y string) bool {
					return x < y
				})

			return res, nil
		}

		head = tail + 1
		tail += batch
	}
}

func QueryAccountBatchByID(batch int, dbo *db.DBMySQL) ([]string, error) {

	syntax := "SELECT addr.address FROM `address` addr WHERE addr.id BETWEEN ? AND ?;"

	res, err := batchAccountByID(batch, syntax, dbo)
	if err != nil {
		return []string{}, nil
	}

	return res, nil
}

func QueryAddressCount(dbo *db.DBMySQL) (int, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return -1, err
	}

	syntax := "SELECT COUNT(*) FROM address;"

	var addrCnt int

	err = conn.QueryRow(syntax).Scan(&addrCnt)
	if err != nil {
		return -1, err
	}

	return addrCnt, nil
}

func QueryAddressCountByAddress(addr string, dbo *db.DBMySQL) (int, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return -1, err
	}

	syntax := fmt.Sprintf("SELECT count(*) AS cnt FROM address a WHERE a.address = '%s';", addr)
	var addrCnt int

	conn.QueryRow(syntax).Scan(&addrCnt)

	return addrCnt, nil
}

func batchAccountWithLimit(batch int, syntax string, dbo *db.DBMySQL) ([]string, error) {
	var res []string

	conn, err := dbo.Manual()
	if err != nil {
		return res, err
	}

	stmt, err := conn.Prepare(syntax)
	if err != nil {
		return res, err
	}

	head := 0

	for {
		tmp := make([]string, 0, batch)

		rows, err := stmt.Query(head, batch)
		if err != nil {
			return res, err
		}

		for rows.Next() {
			var col string

			err := rows.Scan(&col)
			if err != nil {
				return []string{}, err
			}

			tmp = append(tmp, strings.TrimSpace(col))
		}
		rows.Close()

		res = append(res, tmp...)

		if len(tmp) < batch {

			ut.ArrPDQSort[string](res, 0, len(res),
				func(x, y string) bool {
					return x < y
				})

			return res, nil
		}

		head += batch
	}
}

func QueryAccountBatchWithLimit(batch int, dbo *db.DBMySQL) ([]string, error) {

	syntax := "SELECT a.address FROM address a LIMIT ?, ?;"

	res, err := batchAccountWithLimit(batch, syntax, dbo)
	if err != nil {
		return []string{}, nil
	}

	return res, nil
}
