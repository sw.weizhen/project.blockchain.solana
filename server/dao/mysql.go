package dao

import (
	db "gitlab.com/sw.weizhen/db.mysql"
)

func QueryMySQLVersion(dbo *db.DBMySQL) (*db.DBResponse, error) {

	syntax := "SELECT VERSION() AS `VER`;"

	res, err := dbo.Query(syntax)
	if err != nil {
		return nil, err
	}

	return res, nil
}
