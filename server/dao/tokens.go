package dao

import (
	"fmt"

	db "gitlab.com/sw.weizhen/db.mysql"
)

// Deprecated: unsafe, ambiguous return value
func QueryTokenCryptoType(contractAddr string, dbo *db.DBMySQL) (string, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return "", err
	}

	syntax := "SELECT t.crypto_type AS cryptoType FROM tokens t WHERE t.contract_addr = ?;"

	var cryptoType string

	err = conn.QueryRow(syntax, contractAddr).Scan(&cryptoType)
	if err != nil {
		return "", err
	}

	return cryptoType, nil
}

func QueryTokens(dbo *db.DBMySQL) (map[string]string, error) {

	syntax := "SELECT t.crypto_type AS CryptoType, t.contract_addr AS ContractAddr FROM tokens t ORDER BY t.contract_addr ASC;"

	res, err := dbo.Query(syntax)
	if err != nil {
		return nil, err
	}

	resp := map[string]string{}

	for i := 0; i < int(res.Len); i++ {
		row := res.Resp[i]

		cryptoType := fmt.Sprintf("%s", row["CryptoType"])
		contractAddr := fmt.Sprintf("%s", row["ContractAddr"])

		resp[contractAddr] = cryptoType
	}

	return resp, nil
}

func QueryTokenCount(dbo *db.DBMySQL) (int, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return -1, err
	}

	syntax := "SELECT COUNT(*) FROM tokens;"

	var tokenCnt int

	err = conn.QueryRow(syntax).Scan(&tokenCnt)
	if err != nil {
		return -1, err
	}

	return tokenCnt, nil
}

func QueryTokenCountDistinct(dbo *db.DBMySQL) (int, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return -1, err
	}

	syntax := "SELECT  COUNT(DISTINCT t.contract_addr) FROM tokens t;"

	var tokenCnt int

	err = conn.QueryRow(syntax).Scan(&tokenCnt)
	if err != nil {
		return -1, err
	}

	return tokenCnt, nil
}
