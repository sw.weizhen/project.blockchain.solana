package dao

import (
	"fmt"
	"time"

	db "gitlab.com/sw.weizhen/db.mysql"
)

func InsertBlockHeight(slot int64, dbo *db.DBMySQL) (int64, error) {

	unixTime := time.Now().Unix()

	syntax := fmt.Sprintf("INSERT INTO block_height(create_time, update_time, block_slot)VALUES(%d, %d, %d);", unixTime, unixTime, slot)

	res, err := dbo.Exec(syntax)
	if err != nil {
		return -1, err
	}

	return res, nil
}

func UpdateBlockHeight(id int, slot uint64, dbo *db.DBMySQL) (int64, error) {

	unixTime := time.Now().Unix()

	syntax := fmt.Sprintf("UPDATE block_height SET update_time=%d, block_slot=%d WHERE id=%d;", unixTime, slot, id)

	res, err := dbo.Exec(syntax)
	if err != nil {
		return -1, err
	}

	return res, nil
}

// Deprecated: unsafe, ambiguous return value
func QueryBlockHeight(dbo *db.DBMySQL) (int, uint64, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return -1, 0, err
	}

	syntax := "SELECT bh.id AS ID, bh.block_slot AS blockHeight FROM block_height bh ORDER BY bh.block_slot DESC LIMIT 1;"

	var id int
	var slot uint64

	err = conn.QueryRow(syntax).Scan(&id, &slot)
	if err != nil {
		return -1, 0, err
	}

	return id, slot, nil
}

func QueryBlockHeightCount(dbo *db.DBMySQL) (int, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return 0, err
	}

	syntax := "SELECT COUNT(*) AS CNT FROM block_height;"

	var count int

	err = conn.QueryRow(syntax).Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}
