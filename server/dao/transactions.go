package dao

import (
	"fmt"
	"strings"
	"time"

	db "gitlab.com/sw.weizhen/db.mysql"
	"gitlab.com/sw.weizhen/solana.kit/namespace"
)

func QueryTransactionCount(txHash string, dbo *db.DBMySQL) (int, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return -1, err
	}

	syntax := "SELECT count(*) FROM `transaction` tx WHERE tx.tx_hash = ?;"

	var txCnt int
	conn.QueryRow(syntax, txHash).Scan(&txCnt)

	return txCnt, nil
}

func InsertTransactionsPrepare(txs []Transaction, dbo *db.DBMySQL) error {

	if len(txs) == 0 {
		return nil
	}

	conn, err := dbo.Manual()
	if err != nil {
		return err
	}

	syntax := "INSERT INTO `transaction`(create_time, update_time, tx_type, block_height, transaction_index, tx_hash, crypto_type, chain_type, contract_addr, from_address, to_address, amount, gas_price, fee, fee_crypto, status, notify_status, transaction_time)"
	syntax += "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"

	dbTX, err := conn.Begin()
	if err != nil {
		return err
	}

	stmt, err := dbTX.Prepare(syntax)
	if err != nil {
		dbTX.Rollback()
		return err
	}
	defer stmt.Close()

	for i := 0; i < len(txs); i++ {

		_, err := stmt.Exec(
			txs[i].CreateTime,
			txs[i].UpdateTime,
			txs[i].TxType,
			txs[i].BlockHeight,
			txs[i].TransactionIndex,
			txs[i].TxHash,
			txs[i].CryptoType,
			txs[i].ChainType,
			txs[i].ContractAddr,
			txs[i].FromAddress,
			txs[i].ToAddress,
			txs[i].Amount.String(),
			txs[i].GasPrice.String(),
			txs[i].Fee.String(),
			txs[i].FeeCrypto,
			txs[i].Status,
			txs[i].NotifyStatus,
			txs[i].TransactionTime,
		)

		if err != nil {
			dbTX.Rollback()

			return err
		}
	}

	dbTX.Commit()

	return nil
}

// plaintext
func InsertTransaction(tx Transaction, dbo *db.DBMySQL) (int64, error) {

	var sb strings.Builder
	sb.WriteString("INSERT INTO `transaction`(create_time, update_time, tx_type, block_height, transaction_index, tx_hash, crypto_type, chain_type, contract_addr, from_address, to_address, amount, gas_price, fee, fee_crypto, status, notify_status, transaction_time)")
	sb.WriteString("VALUES(")
	sb.WriteString(fmt.Sprintf("%d,", tx.CreateTime))
	sb.WriteString(fmt.Sprintf("%d,", tx.UpdateTime))
	sb.WriteString(fmt.Sprintf("%d,", tx.TxType))
	sb.WriteString(fmt.Sprintf("%d,", tx.BlockHeight))
	sb.WriteString(fmt.Sprintf("%d,", tx.TransactionIndex))
	sb.WriteString(fmt.Sprintf("'%s',", tx.TxHash))
	sb.WriteString(fmt.Sprintf("'%s',", tx.CryptoType))
	sb.WriteString(fmt.Sprintf("'%s',", tx.ChainType))
	sb.WriteString(fmt.Sprintf("'%s',", tx.ContractAddr))
	sb.WriteString(fmt.Sprintf("'%s',", tx.FromAddress))
	sb.WriteString(fmt.Sprintf("'%s',", tx.ToAddress))
	sb.WriteString(fmt.Sprintf("%s,", tx.Amount.String()))
	sb.WriteString(fmt.Sprintf("%s,", tx.GasPrice.String()))
	sb.WriteString(fmt.Sprintf("%s,", tx.Fee.String()))
	sb.WriteString(fmt.Sprintf("'%s',", tx.FeeCrypto))
	sb.WriteString(fmt.Sprintf("%d,", tx.Status))
	sb.WriteString(fmt.Sprintf("%d,", tx.NotifyStatus))
	sb.WriteString(fmt.Sprintf("%d", tx.TransactionTime))
	sb.WriteString(");")

	res, err := dbo.Exec(sb.String())
	if err != nil {
		return -1, err
	}

	return res, nil
}

func plaintextTransactionsInsert(txs []Transaction) string {

	var sb strings.Builder
	sb.WriteString("INSERT INTO `transaction`(create_time, update_time, tx_type, block_height, transaction_index, tx_hash, crypto_type, chain_type, contract_addr, from_address, to_address, amount, gas_price, fee, fee_crypto, status, notify_status, transaction_time, tx_hash_origin)")
	sb.WriteString("VALUES")

	for i := 0; i < len(txs); i++ {
		sb.WriteString("(")
		sb.WriteString(fmt.Sprintf("%d,", txs[i].CreateTime))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].UpdateTime))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].TxType))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].BlockHeight))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].TransactionIndex))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].TxHash))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].CryptoType))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].ChainType))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].ContractAddr))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].FromAddress))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].ToAddress))
		sb.WriteString(fmt.Sprintf("%s,", txs[i].Amount.String()))
		sb.WriteString(fmt.Sprintf("%s,", txs[i].GasPrice.String()))
		sb.WriteString(fmt.Sprintf("%s,", txs[i].Fee.String()))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].FeeCrypto))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].Status))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].NotifyStatus))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].TransactionTime))
		sb.WriteString(fmt.Sprintf("'%s'", txs[i].TxHashOrigin))
		sb.WriteString("),")
	}

	// syntax := sb.String()[:len(sb.String())-1] + ";"
	syntax := sb.String()
	syntax = syntax[:len(syntax)-1] + ";"

	return syntax
}

func TXUpdWithdrawAndInsTransaction(transactions []Transaction, dbo *db.DBMySQL) error {

	if len(transactions) == 0 {
		return nil
	}

	conn, err := dbo.Manual()
	if err != nil {
		return err
	}

	// MVCC
	tx, err := conn.Begin()
	if err != nil {
		return err
	}

	// stmt, err := tx.Prepare("SELECT w.id AS ID, w.tx_hash_origin AS TxHashOrg FROM withdraw w WHERE w.tx_hash = ?")
	// if err != nil {
	// 	tx.Rollback()

	// 	return err
	// }
	// defer stmt.Close()

	tmNow := time.Now().Unix()

	var sbUPDWithdraw strings.Builder
	for i := 0; i < len(transactions); i++ {

		transaction := transactions[i]
		if transaction.TxType == namespace.TxTypeWithdraw {

			selWDSyntax := fmt.Sprintf("SELECT w.id AS ID, w.tx_hash_origin AS TxHashOrg FROM withdraw w WHERE w.tx_hash = '%s'", transaction.TxHash)

			var id int
			var txHashOrg string

			err := tx.QueryRow(selWDSyntax).Scan(&id, &txHashOrg)
			if err != nil && !strings.Contains(err.Error(), cst_ERROR_NO_ROWS) {
				tx.Rollback()

				return err
			}

			if id > 0 && len(txHashOrg) > 0 {
				transactions[i].TxHashOrigin = txHashOrg

				updWDSyntax := fmt.Sprintf("UPDATE withdraw SET update_time=%d, has_chain=1 WHERE id=%d;", tmNow, id)
				sbUPDWithdraw.WriteString(updWDSyntax)
			}
		}
	}

	sbUPDWithdrawSyntax := sbUPDWithdraw.String()
	if len(sbUPDWithdrawSyntax) > 0 {

		_, err := tx.Exec(sbUPDWithdrawSyntax)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	insTxSyntax := plaintextTransactionsInsert(transactions)
	if len(insTxSyntax) > 0 {

		_, err := tx.Exec(insTxSyntax)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	tx.Commit()

	return nil
}

// plaintext
func InsertTransactions(txs []Transaction, dbo *db.DBMySQL) (int64, error) {

	if len(txs) == 0 {
		return -1, nil
	}

	var sb strings.Builder
	sb.WriteString("INSERT INTO `transaction`(create_time, update_time, tx_type, block_height, transaction_index, tx_hash, crypto_type, chain_type, contract_addr, from_address, to_address, amount, gas_price, fee, fee_crypto, status, notify_status, transaction_time)")
	sb.WriteString("VALUES")

	for i := 0; i < len(txs); i++ {
		sb.WriteString("(")
		sb.WriteString(fmt.Sprintf("%d,", txs[i].CreateTime))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].UpdateTime))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].TxType))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].BlockHeight))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].TransactionIndex))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].TxHash))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].CryptoType))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].ChainType))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].ContractAddr))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].FromAddress))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].ToAddress))
		sb.WriteString(fmt.Sprintf("%s,", txs[i].Amount.String()))
		sb.WriteString(fmt.Sprintf("%s,", txs[i].GasPrice.String()))
		sb.WriteString(fmt.Sprintf("%s,", txs[i].Fee.String()))
		sb.WriteString(fmt.Sprintf("'%s',", txs[i].FeeCrypto))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].Status))
		sb.WriteString(fmt.Sprintf("%d,", txs[i].NotifyStatus))
		sb.WriteString(fmt.Sprintf("%d", txs[i].TransactionTime))
		sb.WriteString("),")
	}

	syntax := sb.String()
	syntax = syntax[:len(syntax)-1] + ";"

	res, err := dbo.Exec(syntax)
	if err != nil {
		return -1, err
	}

	return res, nil
}
