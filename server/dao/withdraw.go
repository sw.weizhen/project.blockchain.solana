package dao

import (
	"fmt"
	"time"

	db "gitlab.com/sw.weizhen/db.mysql"
)

// Deprecated: unsafe, ambiguous return value
func QueryWithdrawByTxHash(txHash string, dbo *db.DBMySQL) (string, error) {

	conn, err := dbo.Manual()
	if err != nil {
		return "", err
	}

	syntax := fmt.Sprintf("SELECT w.tx_hash_origin AS TxHashOrg FROM withdraw w WHERE w.tx_hash = '%s';", txHash)

	var TxHashOrg string

	err = conn.QueryRow(syntax).Scan(&TxHashOrg)
	if err != nil {
		return "", err
	}

	return TxHashOrg, nil
}

func UpdateWithdraw(txHash string, dbo *db.DBMySQL) (int64, error) {

	unixTime := time.Now().Unix()

	syntax := fmt.Sprintf("UPDATE withdraw SET has_chain = 1, update_time = %d WHERE tx_hash_origin = '%s' OR tx_hash = '%s';", unixTime, txHash, txHash)

	res, err := dbo.Exec(syntax)
	if err != nil {
		return -1, err
	}

	return res, nil
}
