package dao

import (
	"github.com/shopspring/decimal"
)

type Transaction struct {
	Memo              string
	TxHashOrigin      string
	FeeCrypto         string
	TxHash            string
	CryptoType        string
	ChainType         string
	ContractAddr      string
	FromAddress       string
	ToAddress         string
	BlockHeight       uint64
	Gas               int64
	GasUsed           int64
	TransactionTime   int64
	CreateTime        int64
	UpdateTime        int64
	Confirm           int64
	TxType            int
	TransactionIndex  int
	Status            int
	NotifyStatus      int
	RiskControlStatus int
	Amount            decimal.Decimal
	GasPrice          decimal.Decimal
	Fee               decimal.Decimal
}
