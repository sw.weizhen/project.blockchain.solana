package server

const (
	CST_VERSION = "0.0.3"
)

const (
	cst_LOG_MIN_KEEPDAYS = 3
	cst_LOG_KEEPDAYS     = 7
)

const (
	cst_TIMEOUT_GET_BLOCK_HEIGHT = 10
	cst_TIMEOUT_SHUTDOWN         = 13
)

const (
	cst_RETRY_CHANNEL_SIZE      = 512
	cst_RETRY_DELAY_MILLISECOND = 5000
)

const (
	cst_ERROR_BLOCK_NOT_AVAILABLE  = "Block not available"
	cst_ERROR_BLOCK_SKIPPED        = "skipped, or missing"
	cst_ERROR_REMOTE_FAIL_GETBLOCK = "rpc call getBlock"
	cst_ERROR_BLOCK_CLEAN_UP       = "cleaned up"
	// cst_ERROR_REMOTE_FAIL_GETLATEESTBLOCKHASH = "rpc call getLatestBlockhash"
)
