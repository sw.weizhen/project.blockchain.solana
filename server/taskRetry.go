package server

import (
	"context"
	"strings"
	"time"

	zl "project.blockchain.solana/component/zerolog"
	"project.blockchain.solana/server/service"
)

func retryUnavailableSlot(servCtx context.Context) {

LBL_RETRY:
	for {
		select {
		case <-servCtx.Done():
			zl.L.Info().Msg("retryUnavailableSlot() -> signal notify received, waiting for done ...")

			break LBL_RETRY

		case blockRetry := <-opvChannelRetry:
			go retryBlock(blockRetry)
		}
	}

	zl.L.Info().Msg("retryUnavailableSlot() -> context done")
}

func retryBlock(blockRetry service.BlockRetry) {

	retrySlot := blockRetry.Slot
	retryCnt := blockRetry.Count

	if retryCnt >= opvCFG.Server.Retry {
		zl.L.Warn().Msgf("retryBlock() -> slot %d discard, number of retries(%d) exceeded", retrySlot, retryCnt)
		return
	}

	<-time.After(time.Millisecond * time.Duration(cst_RETRY_DELAY_MILLISECOND*retryCnt))

	ctxGetBlock, cancel := context.WithTimeout(context.TODO(), time.Duration(opvCFG.Server.BlockRPCTimeoutSec)*time.Second)
	defer cancel()

	block, err := service.GetBlock(ctxGetBlock, retrySlot, OpvC)
	if err != nil {

		zl.L.Error().Msgf("retryBlock::GetBlock(%d) -> %v", retrySlot, err)

		if strings.Contains(err.Error(), cst_ERROR_BLOCK_CLEAN_UP) {
			zl.L.Warn().Msgf("retryBlock() -> slot %d discard, does not exist on node(cleaned up)", retrySlot)

			return
		}

		if strings.Contains(err.Error(), cst_ERROR_BLOCK_NOT_AVAILABLE) || strings.Contains(err.Error(), cst_ERROR_REMOTE_FAIL_GETBLOCK) {

			opvChannelRetry <- service.BlockRetry{
				Slot:  retrySlot,
				Count: retryCnt + 1,
			}

			return
		}

		return
	}

	blockTime := block.BlockTime
	blockTXs := block.Transactions

	_, err = service.HandleBlockTransactions(blockTXs, retrySlot, blockTime, OpvDB)
	if err != nil {
		zl.L.Error().Msgf("retryBlock::HandleBlockTransactions(%d) -> %v", retrySlot, err)
		return
	}

	zl.L.Info().Msgf("retryBlock slot %d successfully", retrySlot)
}
