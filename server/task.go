package server

import (
	"context"
	"runtime/debug"
	"strings"
	"sync"
	"time"

	zl "project.blockchain.solana/component/zerolog"
	"project.blockchain.solana/server/dao"
	"project.blockchain.solana/server/service"

	ut "project.blockchain.solana/component/utils"
)

func chimera(ctxGetBlock context.Context, slot uint64, wg *sync.WaitGroup) {

	defer func() {
		if err := recover(); err != nil {
			zl.L.Error().Msgf("chimera() -> %v, debug.Stack: %s", err, debug.Stack())
		}
	}()

	defer wg.Done()

	block, err := service.GetBlock(ctxGetBlock, slot, OpvC)
	if err != nil {

		if strings.Contains(err.Error(), cst_ERROR_BLOCK_SKIPPED) {
			return
		}

		zl.L.Error().Msgf("chimera::GetBlock(%d) -> %v", slot, err)

		if strings.Contains(err.Error(), cst_ERROR_BLOCK_CLEAN_UP) {
			zl.L.Error().Msgf("chimera::GetBlock(%d) -> slot discard, does not exist on node(cleaned up)", slot)

			return
		}

		if strings.Contains(err.Error(), cst_ERROR_BLOCK_NOT_AVAILABLE) || strings.Contains(err.Error(), cst_ERROR_REMOTE_FAIL_GETBLOCK) {

			if len(opvChannelRetry) == cap(opvChannelRetry) {

				zl.L.Warn().Msgf("chimera::GetBlock(%d) -> slot %d discard, opvChannel full", slot, slot)

				return
			}

			opvChannelRetry <- service.BlockRetry{
				Slot:  slot,
				Count: 1,
			}

			return
		}

		return
	}

	blockTime := block.BlockTime
	blockTXs := block.Transactions

	slot, err = service.HandleBlockTransactions(blockTXs, slot, blockTime, OpvDB)
	if err != nil {

		zl.L.Error().Msgf("chimera::HandleBlockTransactions(%d) -> %v", slot, err)

		return
	}
}

func askBlockHeight() {

	defer func() {
		if err := recover(); err != nil {
			zl.L.Error().Msgf("askBlockHeight() -> %v, debug.Stack: %s", err, debug.Stack())
		}
	}()

	t1 := ut.TmrNow()

	_, err := service.ReloadAccount(opvCFG.RDBMS.Mysql.SelectBatch, OpvDB)
	if err != nil {
		zl.L.Error().Msgf("askBlockHeight::ReloadAccount() -> %v", err)

		return
	}

	_, err = service.ReloadToken(OpvDB)
	if err != nil {
		zl.L.Error().Msgf("askBlockHeight::ReloadToken() -> %v", err)

		return
	}

	var rngTimeout int = 10
	head, tail, slot, diff, id, pass := service.AssertBlockRNG(opvCFG.Server.BlockHeightLimit, rngTimeout, OpvC, OpvDB)
	if !pass {
		return
	}

	ctxGetBlock, cancelGetBlock := context.WithTimeout(context.TODO(), time.Duration(opvCFG.Server.BlockRPCTimeoutSec)*time.Second)

	var wg *sync.WaitGroup = &sync.WaitGroup{}

	for slotNum := head; slotNum <= tail; slotNum++ {
		wg.Add(1)

		go chimera(ctxGetBlock, slotNum, wg)
	}

	wg.Wait()

	cancelGetBlock()

	_, err = dao.UpdateBlockHeight(id, tail, OpvDB)
	if err != nil {
		zl.L.Error().Msgf("askBlockHeight::UpdateBlockHeight() -> %v", err)

		return
	}

	proc := (tail - head + 1)
	cost := ut.TmrCost(t1)
	if diff >= opvCFG.Server.BlockHeightLimit {
		zl.L.Warn().Msgf("proc %d [%d, %d] diff:%d(%d/%d) cost:%d", slot, head, tail, diff, proc, opvCFG.Server.BlockHeightLimit, cost)

	} else {
		zl.L.Debug().Msgf("proc %d [%d, %d] diff:%d(%d/%d) cost:%d", slot, head, tail, diff, proc, opvCFG.Server.BlockHeightLimit, cost)

	}

}

func pollBlockHeight(servCtx context.Context) {

LBL:
	for {
		select {
		case <-servCtx.Done():

			zl.L.Info().Msg("pollBlockHeight() -> signal notify received, waiting for done ...")

			break LBL

		default:
			askBlockHeight()
		}
	}

	zl.L.Info().Msg("pollBlockHeight() -> context done")
}
