package server

import (
	"os"

	"gopkg.in/yaml.v3"
)

const (
	cst_CFG_DEFAULT_PATH string = "config.yml"
	cst_CFG_LOG_PATH     string = "/log"
)

type serverCfg struct {
	Endpoint           string `yaml:"endpoint"`
	Tag                string `yaml:"tag"`
	Cluster            string `yaml:"cluster"`
	BlockHeightLimit   uint64 `yaml:"blockHeightLimit"`
	BlockRPCTimeoutSec int    `yaml:"blockRPCTimeoutSec"`
	Port               int    `yaml:"port"`
	Retry              int    `yaml:"retry"`
}

type logCfg struct {
	Level   string `yaml:"level"`
	MaxDays int    `yaml:"maxDays"`
	MaxSize int    `yaml:"maxSize"`
}

type rdbmsCfg struct {
	Host                string `yaml:"host"`
	User                string `yaml:"user"`
	Pass                string `yaml:"pass"`
	Charset             string `yaml:"charset"`
	Database            string `yaml:"database"`
	Port                int    `yaml:"port"`
	MaxIdleConns        int    `yaml:"maxIdleConns"`
	MaxOpenConns        int    `yaml:"maxOpenConns"`
	MaxConnLifeTimeMins int    `yaml:"maxConnLifeTimeMins"`
	SelectBatch         int    `yaml:"selectBatch"`
	InsertBatch         int    `yaml:"insertBatch"`
}

type dbsCfg struct {
	Mysql rdbmsCfg `yaml:"mysql"`
}

type BootCfg struct {
	Server serverCfg `yaml:"server"`
	Log    logCfg    `yaml:"log"`
	RDBMS  dbsCfg    `yaml:"rdbms"`
}

func readYMLFile(path ...string) (*BootCfg, error) {

	p := cst_CFG_DEFAULT_PATH
	if len(path) != 0 {
		p = path[0]
	}

	cfg := &BootCfg{}

	ymlF, err := os.ReadFile(p)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(ymlF, cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
