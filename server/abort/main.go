package main

import (
	"fmt"

	"project.blockchain.solana/server"
	"project.blockchain.solana/server/dao"
)

func main() {
	fmt.Println("exam::main() -> entry")

	server.Boot()

	addrs, err := dao.QueryAccountBatchWithLimit(5, server.OpvDB)
	if err != nil {
		fmt.Printf("dao fail: %v\n", err)
	}

	for _, addr := range addrs {
		fmt.Println(addr)
	}

}
