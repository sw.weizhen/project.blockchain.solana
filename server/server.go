package server

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	zl "project.blockchain.solana/component/zerolog"
	"project.blockchain.solana/server/service"
	// rdbmsmysql "gitlab.com/sw.weizhen/db.mysql"
	// cr "gitlab.com/sw.weizhen/solana.kit/caller"
	// "project.blockchain.solana/server/service"
)

type SolServer struct {
	Endpoint string
	Tag      string
	Port     int

	solConfig *BootCfg
	// dbo       *rdbmsmysql.DBMySQL
	// chlRetry  chan service.BlockRetry
	// rpc       *cr.Caller
}

func (ref *SolServer) GetConfig() *BootCfg {
	return ref.solConfig
}

// func (ref *SolServer) GetDB() *rdbmsmysql.DBMySQL {
// 	return ref.dbo
// }

func (ref *SolServer) Run() {

	err := service.AssertBlockHeight(cst_TIMEOUT_GET_BLOCK_HEIGHT, OpvC, OpvDB)
	if err != nil {
		log.Fatalf("service.AssertBlockHeight, err: %v", err)
	}

	sig := make(chan os.Signal, 1)

	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	servCtx, servCancel := context.WithCancel(context.TODO())

	go retryUnavailableSlot(servCtx)
	go pollBlockHeight(servCtx)

	sg := <-sig

	zl.L.Info().Msgf("shutdown server ... signal: %v(%d)", sg, sg)

	servCancel()

	zl.L.Info().Msgf("wait %d seconds", cst_TIMEOUT_SHUTDOWN)

	<-time.After(time.Second * time.Duration(cst_TIMEOUT_SHUTDOWN))

	zl.L.Info().Msgf("server %s done", ref.Tag)

	// os.Exit(0)
}
