package server

import (
	"flag"
	"log"

	rdbmsmysql "gitlab.com/sw.weizhen/db.mysql"
	cr "gitlab.com/sw.weizhen/solana.kit/caller"
	zl "project.blockchain.solana/component/zerolog"

	"project.blockchain.solana/server/dao"
	"project.blockchain.solana/server/service"
)

var (
	configPath string
	// port       int
)

func init() {
	flag.StringVar(&configPath, "configPath", "config.yml", "config path")
	// flag.IntVar(&port, "port", 8850, "port")
}

func deployCfg(path ...string) (*BootCfg, error) {

	cfgPath := cst_CFG_DEFAULT_PATH

	if len(path) > 0 {
		cfgPath = path[0]
	}

	instance, err := readYMLFile(cfgPath)
	if err != nil {
		return nil, err
	}

	return instance, nil
}

func deployDB(solServ *SolServer) (*rdbmsmysql.DBMySQL, error) {

	user := solServ.solConfig.RDBMS.Mysql.User
	pass := solServ.solConfig.RDBMS.Mysql.Pass
	host := solServ.solConfig.RDBMS.Mysql.Host
	port := solServ.solConfig.RDBMS.Mysql.Port
	dtbs := solServ.solConfig.RDBMS.Mysql.Database
	charset := solServ.solConfig.RDBMS.Mysql.Charset

	maxOpenConns := solServ.solConfig.RDBMS.Mysql.MaxOpenConns
	maxIdleConns := solServ.solConfig.RDBMS.Mysql.MaxIdleConns
	maxLifetimeMinute := solServ.solConfig.RDBMS.Mysql.MaxConnLifeTimeMins

	dbo := rdbmsmysql.New(user, pass, host, port, dtbs, charset, maxOpenConns, maxIdleConns, maxLifetimeMinute)

	return dbo, dbo.Err
}

func Boot() *SolServer {

	flag.Parse()

	cfg, err := deployCfg(configPath)
	if err != nil {
		log.Fatalf("Boot()::deploy config fail: %v", err)
	}

	err = zl.PrepareLog(cfg.Log.Level, cfg.Log.MaxSize, cfg.Log.MaxDays)
	if err != nil {
		log.Fatalf("Boot()::deploy logger fail: %v", err)
	}

	zl.L.Info().Msg("Boot()::read config done")

	if cfg.Log.MaxDays <= cst_LOG_MIN_KEEPDAYS {
		cfg.Log.MaxDays = cst_LOG_KEEPDAYS
	}

	zl.L.Info().Msgf("server tag: %s, version: v%s", cfg.Server.Tag, CST_VERSION)
	zl.L.Info().Msgf("server cluster slot: %s", cfg.Server.Cluster)
	zl.L.Info().Msgf("server third-party endpoint: %s", cfg.Server.Endpoint)
	zl.L.Info().Msgf("log max age: %d days", cfg.Log.MaxDays)
	zl.L.Info().Msgf("log max size %d MB", cfg.Log.MaxSize)
	zl.L.Info().Msgf("db query batch: %d", cfg.RDBMS.Mysql.SelectBatch)
	zl.L.Info().Msgf("db insert batch: %d", cfg.RDBMS.Mysql.InsertBatch)

	opvCFG = cfg

	solServer := &SolServer{
		Endpoint:  cfg.Server.Endpoint,
		Tag:       cfg.Server.Tag,
		Port:      cfg.Server.Port,
		solConfig: cfg,
	}

	opvChannelRetry = make(chan service.BlockRetry, cst_RETRY_CHANNEL_SIZE)
	zl.L.Info().Msgf("opvChannelRetry size: %d", cst_RETRY_CHANNEL_SIZE)

	db, err := deployDB(solServer)
	if err != nil {
		log.Fatalf("Boot()::deploy database fail: %v", err)
	}

	res, err := dao.QueryMySQLVersion(db)
	if err != nil {
		log.Fatalf("Boot()::get mysql version fail: %v", err)
	}

	if res.Len > 0 {
		zl.L.Info().Msgf("Boot()::mysql version: %s", res.Resp[0]["VER"])
	}

	OpvDB = db

	service.DataAccounts, err = dao.QueryAccountBatchWithLimit(solServer.solConfig.RDBMS.Mysql.SelectBatch, OpvDB)
	if err != nil {
		log.Fatalf("Boot()::preload addresses fail: %v", err)
	}

	zl.L.Info().Msgf("Boot()::service.DataAccounts, data len:%d", len(service.DataAccounts))

	service.DataTokens, err = dao.QueryTokens(OpvDB)
	if err != nil {
		log.Fatalf("Boot()::preload tokens fail: %v", err)
	}

	zl.L.Info().Msgf("Boot()::service.DataTokens, data len:%d", len(service.DataTokens))

	OpvC = cr.New(solServer.Endpoint)

	return solServer
}
