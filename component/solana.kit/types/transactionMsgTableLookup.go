package types

import "encoding/json"

type AddressTableLookup struct {
	AccountKey      AccountKey `json:"accountKey"`
	WritableIndexes Uint8s     `json:"writableIndexes"`
	ReadonlyIndexes Uint8s     `json:"readonlyIndexes"`
}

type Uint8s []uint8

func (ref Uint8s) MarshalJSON() ([]byte, error) {

	out := make([]uint16, len(ref))
	for i, val := range ref {
		out[i] = uint16(val)
	}

	return json.Marshal(out)
}

type AddressTableLookups []AddressTableLookup

func (ref AddressTableLookups) NumLookups() int {

	cnt := 0
	for _, lookup := range ref {
		cnt += len(lookup.ReadonlyIndexes)
		cnt += len(lookup.WritableIndexes)
	}

	return cnt
}

func (ref AddressTableLookups) NumWritableLookups() int {

	cnt := 0
	for _, lookup := range ref {
		cnt += len(lookup.WritableIndexes)
	}

	return cnt
}

func (ref AddressTableLookups) GetTableIDs() AccountKeys {

	if ref == nil {
		return nil
	}

	ids := make(AccountKeys, 0)
	for _, lookup := range ref {
		ids.UniqueAppend(lookup.AccountKey)
	}

	return ids
}
