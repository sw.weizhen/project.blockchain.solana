package types

import (
	"github.com/shopspring/decimal"
)

type AddressAmount struct {
	Address string
	Amount  decimal.Decimal
}
