package types

type LatestBlockhash struct {
	SolContext
	Value *LatestBlockhasHeight `json:"value"`
}

type LatestBlockhasHeight struct {
	Blockhash            string `json:"blockhash"`
	LastValidBlockHeight uint64 `json:"lastValidBlockHeight"`
}
