package types

type Context struct {
	Slot uint64 `json:"slot"`
}

type SolContext struct {
	Context Context `json:"context"`
}

type Balance struct {
	SolContext
	Value uint64 `json:"value"`
}
