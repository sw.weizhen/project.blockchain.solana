package types

import (
	"bytes"
	"crypto/ed25519"
	"encoding/json"
	"fmt"
	"sort"

	"filippo.io/edwards25519"
	"github.com/mr-tron/base58"
)

const (
	acntkLen = 32
)

type AccountKey [acntkLen]byte

func ConformAccountKeyBytes(in []byte) (pubk AccountKey) {

	cnt := len(in)
	if cnt == 0 {
		return
	}

	max := acntkLen
	if cnt < max {
		max = cnt
	}

	copy(pubk[:], in[0:max])

	return
}

func ConformAccountKeyBase58(in string) (pubk AccountKey, err error) {

	b58D, err := base58.Decode(in)
	if err != nil {
		return pubk, fmt.Errorf("decode: %w", err)
	}

	if len(b58D) != acntkLen {
		return pubk, fmt.Errorf("invalid length(%d) %d", acntkLen, len(b58D))
	}

	copy(pubk[:], b58D)

	return
}

func (ref AccountKey) Equals(pubk AccountKey) bool {
	return ref == pubk
}

func (ref AccountKey) Ptr() *AccountKey {
	return &ref
}

func (ref AccountKey) Bytes() []byte {
	return []byte(ref[:])
}

func (ref AccountKey) IsCurve() bool {
	_, err := new(edwards25519.Point).SetBytes(ref[:])

	return err == nil
}

func (ref AccountKey) MarshalText() ([]byte, error) {
	return []byte(base58.Encode(ref[:])), nil
}

func (ref *AccountKey) UnmarshalText(in []byte) error {
	return ref.Conform(string(in))
}

func (ref AccountKey) MarshalJSON() ([]byte, error) {
	return json.Marshal(base58.Encode(ref[:]))
}

func (ref *AccountKey) UnmarshalJSON(in []byte) (err error) {

	var jU string
	if err := json.Unmarshal(in, &jU); err != nil {
		return err
	}

	*ref, err = ConformAccountKeyBase58(jU)
	if err != nil {
		return fmt.Errorf("invalid public key %q: %w", jU, err)
	}

	return
}

func (ref AccountKey) Verify(msg []byte, sig Signature) bool {
	return ed25519.Verify(ed25519.PublicKey(ref[:]), msg, sig[:])
}

func (ref AccountKey) IsEmpty() bool {
	return ref == AccountKey{}
}

func (ref *AccountKey) Conform(in string) (err error) {

	*ref, err = ConformAccountKeyBase58(in)
	if err != nil {
		return fmt.Errorf("invalid public key %s: %w", in, err)
	}

	return
}

func (ref AccountKey) String() string {
	return base58.Encode(ref[:])
}

type AccountKeys []AccountKey

func (ref *AccountKeys) UniqueAppend(ak AccountKey) bool {

	if !ref.Has(ak) {
		ref.Append(ak)
		return true
	}

	return false
}

func (ref *AccountKeys) Append(ak ...AccountKey) {
	*ref = append(*ref, ak...)
}

func (ref AccountKeys) Has(ak AccountKey) bool {
	for _, key := range ref {
		if key.Equals(ak) {
			return true
		}
	}
	return false
}

func (ref AccountKeys) Len() int {
	return len(ref)
}

func (ref AccountKeys) Less(i, j int) bool {
	return bytes.Compare(ref[i][:], ref[j][:]) < 0
}

func (ref AccountKeys) Swap(i, j int) {
	ref[i], ref[j] = ref[j], ref[i]
}

func (ref AccountKeys) Sort() {
	sort.Sort(ref)
}

func (ref AccountKeys) Dedupe() AccountKeys {

	ref.Sort()

	deduped := make(AccountKeys, 0)
	for i := 0; i < len(ref); i++ {
		if i == 0 || !ref[i].Equals(ref[i-1]) {
			deduped = append(deduped, ref[i])
		}
	}

	return deduped
}

func (ref AccountKeys) Contains(ak AccountKey) bool {

	for _, key := range ref {
		if key.Equals(ak) {
			return true
		}
	}

	return false
}

func (ref AccountKeys) ContainsAll(aks AccountKeys) bool {

	for _, ak := range aks {
		if !ref.Contains(ak) {
			return false
		}
	}

	return true
}

func (ref AccountKeys) ContainsAny(aks ...AccountKey) bool {

	for _, ak := range aks {
		if ref.Contains(ak) {
			return true
		}
	}

	return false
}

func (ref AccountKeys) ToBase58() []string {

	out := make([]string, len(ref))
	for i, ak := range ref {
		out[i] = ak.String()
	}

	return out
}

func (ref AccountKeys) ToBytes() [][]byte {

	out := make([][]byte, len(ref))
	for i, ak := range ref {
		out[i] = ak.Bytes()
	}

	return out
}

func (ref AccountKeys) Ptrs() []*AccountKey {

	out := make([]*AccountKey, len(ref))
	for i, ak := range ref {
		out[i] = ak.Ptr()
	}

	return out
}

func (ref AccountKeys) Removed(aks AccountKeys) AccountKeys {

	var diff AccountKeys
	for _, pubkey := range ref {
		if !aks.Contains(pubkey) {
			diff = append(diff, pubkey)
		}
	}

	return diff.Dedupe()
}

func (ref AccountKeys) Added(b AccountKeys) AccountKeys {
	return b.Removed(ref)
}

func (ref AccountKeys) Intersect(next AccountKeys) AccountKeys {

	var intersect AccountKeys
	for _, ak := range ref {
		if next.Contains(ak) {
			intersect = append(intersect, ak)
		}
	}

	return intersect.Dedupe()
}

func (ref AccountKeys) Equals(aks AccountKeys) bool {

	if len(ref) != len(aks) {
		return false
	}

	for i, ak := range ref {
		if !ak.Equals(aks[i]) {
			return false
		}
	}

	return true
}

func (ref AccountKeys) Same(aks AccountKeys) bool {

	if len(ref) != len(aks) {
		return false
	}

	for _, ak := range ref {
		if !aks.Contains(ak) {
			return false
		}
	}

	return true
}

func (ref AccountKeys) Split(size int) []AccountKeys {

	divided := make([]AccountKeys, 0)
	if len(ref) == 0 || size < 1 {
		return divided
	}

	if len(ref) == 1 {
		return append(divided, ref)
	}

	for i := 0; i < len(ref); i += size {
		end := i + size

		if end > len(ref) {
			end = len(ref)
		}

		divided = append(divided, ref[i:end])
	}

	return divided
}

func (ref AccountKeys) Last() *AccountKey {

	if len(ref) == 0 {
		return nil
	}

	return ref[len(ref)-1].Ptr()
}

func (ref AccountKeys) First() *AccountKey {

	if len(ref) == 0 {
		return nil
	}

	return ref[0].Ptr()
}

func (ref AccountKeys) GetAddedRemoved(aks AccountKeys) (AccountKeys, AccountKeys) {
	return aks.Removed(ref), ref.Removed(aks) // return added, removed
}

func GetAddedRemovedAccountKeys(prev AccountKeys, next AccountKeys) (added AccountKeys, removed AccountKeys) {

	added = make(AccountKeys, 0)
	removed = make(AccountKeys, 0)

	for _, prev := range prev {
		if !next.Has(prev) {
			removed = append(removed, prev)
		}
	}

	for _, nx := range next {
		if !prev.Has(nx) {
			added = append(added, nx)
		}
	}

	return
}
