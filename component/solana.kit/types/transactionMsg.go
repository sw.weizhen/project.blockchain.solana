package types

import (
	"fmt"
)

type Message struct {
	MsgAccountKeys         AccountKeys         `json:"accountKeys"`
	MsgHeader              MessageHeader       `json:"header"`
	MsgRecentBlockhash     RBHash              `json:"recentBlockhash"`
	MsgInstructions        Instructions        `json:"instructions"`
	MsgAddressTableLookups AddressTableLookups `json:"addressTableLookups"`

	addressTables map[AccountKey]AccountKeys
	version       MessageVersion
	resolved      bool
}

type MessageVersion int

const (
	MessageVersionLegacy MessageVersion = 0
	MessageVersionV0     MessageVersion = 1
)

func (ref Message) preconditions() error {
	if ref.version != MessageVersionLegacy && ref.MsgAddressTableLookups.NumLookups() > 0 && (ref.addressTables == nil || len(ref.addressTables) == 0) {
		return fmt.Errorf("cannot build account meta list without address tables")
	}

	return nil
}

func (ref Message) GetAddressTableLookupAccounts() (AccountKeys, error) {

	err := ref.preconditions()
	if err != nil {
		return nil, err
	}

	var writable AccountKeys
	var readonly AccountKeys

	for _, lookup := range ref.MsgAddressTableLookups {
		table, ok := ref.addressTables[lookup.AccountKey]

		if !ok {
			return writable, fmt.Errorf("address table lookup not found for account: %s", lookup.AccountKey)
		}

		for _, idx := range lookup.WritableIndexes {
			if int(idx) >= len(table) {
				return writable, fmt.Errorf("address table lookup index out of range: %d", idx)
			}
			writable = append(writable, table[idx])
		}

		for _, idx := range lookup.ReadonlyIndexes {
			if int(idx) >= len(table) {
				return writable, fmt.Errorf("address table lookup index out of range: %d", idx)
			}
			readonly = append(readonly, table[idx])
		}
	}

	return append(writable, readonly...), nil
}

func (ref Message) GetAllKeys() (keys AccountKeys, err error) {
	if ref.resolved {
		return ref.MsgAccountKeys, nil
	}

	atlAccounts, err := ref.GetAddressTableLookupAccounts()
	if err != nil {
		return keys, err
	}

	return append(ref.MsgAccountKeys, atlAccounts...), nil
}

func (ref Message) isWritableInLookups(idx int) bool {
	if idx < ref.numStaticAccounts() {
		return false
	}

	return idx-ref.numStaticAccounts() < ref.MsgAddressTableLookups.NumWritableLookups()
}

func (ref Message) NumLookups() int {
	if ref.MsgAddressTableLookups == nil {
		return 0
	}

	return ref.MsgAddressTableLookups.NumLookups()
}

func (ref Message) numStaticAccounts() int {
	if !ref.resolved {
		return len(ref.MsgAccountKeys)
	}

	return len(ref.MsgAccountKeys) - ref.NumLookups()
}

func (ref Message) IsWritable(account AccountKey) (bool, error) {

	err := ref.preconditions()
	if err != nil {
		return false, err
	}

	accountKeys, err := ref.GetAllKeys()
	if err != nil {
		return false, err
	}

	index := 0
	found := false
	for idx, acc := range accountKeys {
		if acc.Equals(account) {
			found = true
			index = idx
		}
	}

	if !found {
		return false, err
	}

	h := ref.MsgHeader

	if index >= ref.numStaticAccounts() {
		return ref.isWritableInLookups(index), nil

	} else if index >= int(h.NumRequiredSignatures) {

		return index-int(h.NumRequiredSignatures) < (ref.numStaticAccounts()-int(h.NumRequiredSignatures))-int(h.NumReadonlyUnsignedAccounts), nil
	}

	return index < int(h.NumRequiredSignatures-h.NumReadonlySignedAccounts), nil
}

func (ref Message) IsSigner(ak AccountKey) bool {

	for idx, acc := range ref.MsgAccountKeys {
		if acc.Equals(ak) {
			return idx < int(ref.MsgHeader.NumRequiredSignatures)
		}
	}

	return false
}

func (ref Message) AccountMetaList() (AccountMetas, error) {

	err := ref.preconditions()
	if err != nil {
		return nil, err
	}

	accountKeys, err := ref.GetAllKeys()
	if err != nil {
		return nil, err
	}

	out := make(AccountMetas, len(accountKeys))

	for i, a := range accountKeys {

		isWritable, err := ref.IsWritable(a)
		if err != nil {
			return nil, err
		}

		out[i] = &AccountMeta{
			AKey:       a,
			IsSigner:   ref.IsSigner(a),
			IsWritable: isWritable,
		}
	}

	return out, nil
}
