package types

type Instruction struct {
	ProgramIDIndex uint16   `json:"programIdIndex"`
	Accounts       []uint16 `json:"accounts"`
	Data           Base58   `json:"data"`
}

type Instructions []Instruction
