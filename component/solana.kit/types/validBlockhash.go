package types

type ValidBlockhash struct {
	SolContext
	Value bool `json:"value"`
}
