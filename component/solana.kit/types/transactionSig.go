package types

import (
	"crypto/ed25519"
	"encoding/json"
	"fmt"

	"github.com/mr-tron/base58"
)

const (
	sigLen = 64
)

type Signature [sigLen]byte

func (ref Signature) IsEmpty() bool {
	return ref == Signature{}
}

func (ref Signature) Equals(sig Signature) bool {
	return ref == sig
}

func ConformSignatureBase58(in string) (sig Signature, err error) {

	b58D, err := base58.Decode(in)
	if err != nil {
		return
	}

	if len(b58D) != sigLen {
		err = fmt.Errorf("invalid length(%d) %d", sigLen, len(b58D))
		return
	}

	copy(sig[:], b58D)

	return
}

func ConformSignatureBytes(in []byte) (sig Signature) {

	cnt := len(in)
	if cnt == 0 {
		return
	}

	max := sigLen
	if cnt < max {
		max = cnt
	}

	copy(sig[:], in[0:max])

	return
}

func (ref Signature) MarshalText() ([]byte, error) {
	b58E := base58.Encode(ref[:])
	return []byte(b58E), nil
}

func (ref *Signature) UnmarshalText(in []byte) (err error) {

	tmp, err := ConformSignatureBase58(string(in))
	if err != nil {
		return fmt.Errorf("invalid signature %q: %w", string(in), err)
	}

	*ref = tmp

	return
}

func (ref Signature) MarshalJSON() ([]byte, error) {
	return json.Marshal(base58.Encode(ref[:]))
}

func (p *Signature) UnmarshalJSON(data []byte) (err error) {
	var s string
	err = json.Unmarshal(data, &s)
	if err != nil {
		return
	}

	dat, err := base58.Decode(s)
	if err != nil {
		return err
	}

	if len(dat) != sigLen {
		return fmt.Errorf("invalid length(%d) %d", sigLen, len(dat))
	}

	target := Signature{}
	copy(target[:], dat)
	*p = target
	return
}

func (ref Signature) Verify(acntk AccountKey, msg []byte) bool {
	return ed25519.Verify(acntk[:], msg, ref[:])
}

func (ref Signature) String() string {
	return base58.Encode(ref[:])
}
