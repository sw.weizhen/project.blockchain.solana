package types

import (
	"gitlab.com/sw.weizhen/solana.kit/namespace"
)

// https://solana.com/docs/rpc/http/getblock
type BlockConfigurationObject struct {
	Commitment                     namespace.Commitment
	Encoding                       namespace.Encoding
	TransactionDetails             namespace.TransactionDetails
	MaxSupportedTransactionVersion *uint64
	Rewards                        *bool
}

type Block struct {
	BlockHash         string             `json:"blockhash"`
	PreviousBlockHash string             `json:"previousBlockhash"`
	ParentSlot        uint64             `json:"parentSlot"`
	BlockHeight       uint64             `json:"blockHeight"`
	BlockTime         int64              `json:"blockTime"`
	Signatures        []string           `json:"signatures"`
	Rewards           []BlockReward      `json:"rewards"`
	Transactions      []BlockTransaction `json:"transactions"`
}

type BlockReward struct {
	Pubkey      string `json:"pubkey"`
	RewardType  string `json:"rewardType"`
	PostBalance uint64 `json:"postBalance"`
	Lamports    int64  `json:"lamports"`
	Commission  uint8  `json:"commission"`
}

type BlockTransaction struct {
	Version     interface{}      `json:"version"`
	Transaction interface{}      `json:"transaction"` //Transaction object, either in JSON format or encoded binary data, depending on encoding parameter
	Meta        *TransactionMeta `json:"meta"`
}

type TransactionMeta struct {
	Err                  interface{}              `json:"err"`
	Fee                  uint64                   `json:"fee"`
	LoadedAddresses      BlockLoadedAddresses     `json:"loadedAddresses"`
	LogMessages          []string                 `json:"logMessages"`
	PreBalances          []int64                  `json:"preBalances"`
	PostBalances         []int64                  `json:"postBalances"`
	PreTokenBalances     []BlockTokenBalance      `json:"preTokenBalances"`
	PostTokenBalances    []BlockTokenBalance      `json:"postTokenBalances"`
	Rewards              []BlockReward            `json:"rewards"`
	InnerInstructions    []BlockInnerInstructions `json:"innerInstructions"`
	ReturnData           *BlockReturnData         `json:"returnData"`
	ComputeUnitsConsumed *uint64                  `json:"computeUnitsConsumed"`
}

type BlockReturnData struct {
	ProgramId string      `json:"programId"`
	Data      interface{} `json:"data"`
}

type BlockLoadedAddresses struct {
	ReadOnly []string `json:"readonly"`
	Writable []string `json:"writable"`
}

type BlockInnerInstructions struct {
	Index        uint64        `json:"index"`
	Instructions []interface{} `json:"instructions"`
}

type BlockTokenBalance struct {
	AccountIndex  uint64                   `json:"accountIndex"`
	Mint          string                   `json:"mint"`
	Owner         string                   `json:"owner,omitempty"`
	ProgramId     string                   `json:"programId,omitempty"`
	UITokenAmount BlockTokenAccountBalance `json:"uiTokenAmount"`
}

type BlockTokenAccountBalance struct {
	Amount         string `json:"amount"`
	UIAmountString string `json:"uiAmountString"`
	Decimals       uint8  `json:"decimals"`
}
