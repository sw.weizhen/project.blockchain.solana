package types

import (
	"math"

	"github.com/shopspring/decimal"
)

var (
	SolMul   = decimal.NewFromInt(1000000000)
	MinerFee = decimal.NewFromFloat(0.000005)
)

func ConvertStrBalanceToDecimal(b string, decimals int) decimal.Decimal {
	d, _ := decimal.NewFromString(b)
	return d.Div(decimal.NewFromFloat(math.Pow10(decimals)))
}

func ConvertSolBalanceToDecimal(b uint64) decimal.Decimal {
	return decimal.NewFromInt(int64(b)).Div(SolMul)
}

func ConvertDecimalToSolBalance(b decimal.Decimal) uint64 {
	d, _ := decimal.NewFromString(b.String())
	return uint64(d.Mul(SolMul).IntPart())
}

func ConvertDecimalToSolTokenBalance(b decimal.Decimal, tokenDecimal int) uint64 {
	d, _ := decimal.NewFromString(b.String())
	return uint64(d.Mul(decimal.NewFromInt(int64(math.Pow10(tokenDecimal)))).IntPart())
}
