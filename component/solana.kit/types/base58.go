package types

import (
	"encoding/json"

	"github.com/mr-tron/base58"
)

type Base58 []byte

func (ref Base58) MarshalJSON() ([]byte, error) {
	return json.Marshal(base58.Encode(ref))
}

func (ref *Base58) UnmarshalJSON(data []byte) (err error) {

	var str string
	err = json.Unmarshal(data, &str)
	if err != nil {
		return
	}

	if len(str) == 0 {
		*ref = []byte{}
		return nil
	}

	*ref, err = base58.Decode(str)

	return
}

func (ref Base58) String() string {
	return base58.Encode(ref)
}
