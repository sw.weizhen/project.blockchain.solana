package types

import (
	"encoding/json"
	"fmt"

	"github.com/mr-tron/base58"
)

type RBHash AccountKey

func ConformHashBase58(in string) (RBHash, error) {
	tmp, err := ConformAccountKeyBase58(in)
	if err != nil {
		return RBHash{}, err
	}
	return RBHash(tmp), nil
}

func ConformHashBytes(in []byte) RBHash {
	return RBHash(ConformAccountKeyBytes(in))
}

func (ref RBHash) Equals(in RBHash) bool {
	return ref == in
}

func (ref RBHash) IsEmpty() bool {
	return ref == RBHash{}
}

func (ref RBHash) MarshalText() ([]byte, error) {
	s := base58.Encode(ref[:])
	return []byte(s), nil
}

func (ref *RBHash) UnmarshalText(in []byte) (err error) {

	tmp, err := ConformHashBase58(string(in))
	if err != nil {
		return fmt.Errorf("invalid RBHash %q: %w", string(in), err)
	}

	*ref = tmp
	return
}

func (ref RBHash) MarshalJSON() ([]byte, error) {
	return json.Marshal(base58.Encode(ref[:]))
}

func (ref *RBHash) UnmarshalJSON(in []byte) (err error) {

	var uJ string
	if err := json.Unmarshal(in, &uJ); err != nil {
		return err
	}

	tmp, err := ConformHashBase58(uJ)
	if err != nil {
		return fmt.Errorf("invalid RBHash %q: %w", uJ, err)
	}

	*ref = tmp

	return
}

// func (ref RBHash) Verify() bool {

// }

func (ref RBHash) String() string {
	return base58.Encode(ref[:])
}
