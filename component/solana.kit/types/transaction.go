package types

type Transaction struct {
	Signatures []Signature `json:"signatures"`
	Message    Message     `json:"message"`
}

func (ref *Transaction) AccountMetaList() ([]*AccountMeta, error) {
	return ref.Message.AccountMetaList()
}
