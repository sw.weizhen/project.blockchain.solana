package types

type Version struct {
	SolanaCore string `json:"solana-core"`
	FeatureSet int64  `json:"feature-set"`
}
