package types

type AccountMeta struct {
	AKey       AccountKey
	IsWritable bool
	IsSigner   bool
}

type AccountMetas []*AccountMeta
