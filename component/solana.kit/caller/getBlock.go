package caller

import (
	"context"
	"errors"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
)

func (ref *Caller) GetBlock(ctx context.Context, slot uint64, cfg *types.BlockConfigurationObject) (out *types.Block, err error) {

	cfgObj := map[string]interface{}{}

	if cfg != nil {

		if len(cfg.Commitment) != 0 {
			cfgObj[string(namespace.COMMITMENT)] = cfg.Commitment
		}

		if len(cfg.Encoding) != 0 {
			cfgObj[string(namespace.ENCODING)] = cfg.Encoding
		}

		if len(cfg.TransactionDetails) != 0 {
			cfgObj[string(namespace.TRANSACTION_DETAILS)] = cfg.TransactionDetails
		}

		if cfg.Rewards != nil {
			cfgObj[string(namespace.REWARDS)] = cfg.Rewards
		}

		if cfg.MaxSupportedTransactionVersion != nil {
			cfgObj[string(namespace.MAX_SUPPORTED_TRANSACTION_VERSION)] = cfg.MaxSupportedTransactionVersion
		}
	}

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_GET_BLOCK, []interface{}{slot, cfgObj})

	if err != nil {
		return nil, err
	}

	if out == nil {
		return nil, errors.New("slot not confirmed")
	}

	return
}
