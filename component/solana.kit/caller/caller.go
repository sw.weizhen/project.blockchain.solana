package caller

import (
	"context"
	"time"

	rpc "gitlab.com/sw.weizhen/solana.kit/caller/rpc"
)

const (
	cDEFAULT_MAX_CONNS_PER_HOST      = 9
	cDEFAULT_MAX_IDLE_CONNS_PER_HOST = 9
	cDEFAULT_TIMEOUT                 = time.Minute * 5
	cDEFAULT_KEEP_ALIVE              = time.Minute * 3
)

type Caller struct {
	// endpoint  string
	rpcClient RPCClienter
}

func (ref *Caller) CallRef(ctx context.Context, out interface{}, method string, params []interface{}) error {

	return ref.rpcClient.CallRef(ctx, out, method, params)
}

func NewWithRPCClient(rpcClient RPCClienter) *Caller {
	return &Caller{
		rpcClient: rpcClient,
	}
}

func New(endpoint string) *Caller {

	opts := &rpc.RPCClientOpts{
		HTTPClient: fnNewHTTPClient(),
	}

	return NewWithRPCClient(rpc.NewWithOpts(endpoint, opts))
}
