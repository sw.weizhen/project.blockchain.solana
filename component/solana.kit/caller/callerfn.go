package caller

import (
	"net"
	"net/http"
	"time"

	"github.com/klauspost/compress/gzhttp"
)

func fnNewHTTPClient() *http.Client {

	httpTrans := &http.Transport{

		DialContext: (&net.Dialer{
			Timeout:   cDEFAULT_TIMEOUT,
			KeepAlive: cDEFAULT_KEEP_ALIVE,
			DualStack: true,
		}).DialContext,

		IdleConnTimeout:     cDEFAULT_TIMEOUT,
		MaxConnsPerHost:     cDEFAULT_MAX_CONNS_PER_HOST,
		MaxIdleConnsPerHost: cDEFAULT_MAX_IDLE_CONNS_PER_HOST,
		Proxy:               http.ProxyFromEnvironment,

		ForceAttemptHTTP2:   true,
		TLSHandshakeTimeout: 15 * time.Second,
	}

	return &http.Client{
		Timeout:   cDEFAULT_TIMEOUT,
		Transport: gzhttp.Transport(httpTrans),
	}
}
