package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
)

func (ref *Caller) GetLatestBlockhash(ctx context.Context, commitment namespace.Commitment) (out *types.LatestBlockhash, err error) {

	params := []interface{}{}

	if len(commitment) != 0 {
		params = append(params, map[string]interface{}{string(namespace.COMMITMENT): string(commitment)})
	}

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_GET_LATEST_BLOCK_HASH, params)

	return
}
