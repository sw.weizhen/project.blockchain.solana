package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
)

func (ref *Caller) GetVersion(ctx context.Context) (out *types.Version, err error) {

	err = ref.CallRef(ctx, &out, namespace.METHOD_GET_VERSION, nil)

	return
}
