package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
)

func (ref *Caller) GetBlocks(ctx context.Context, headSlot uint64, tailSlot *uint64, commitment namespace.Commitment) (out types.Blocks, err error) {

	params := []interface{}{headSlot}
	if tailSlot != nil {
		params = append(params, tailSlot)
	}

	if len(commitment) != 0 {
		params = append(params, map[string]interface{}{string(namespace.COMMITMENT): string(commitment)})
	}

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_GET_BLOCKS, params)

	return
}
