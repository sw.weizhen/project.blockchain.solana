package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
)

func (ref *Caller) IsBlockhashValid(ctx context.Context, blockhash string, commitment namespace.Commitment) (out *types.ValidBlockhash, err error) {

	params := []interface{}{blockhash}
	if len(commitment) != 0 {
		params = append(params, map[string]interface{}{string(namespace.COMMITMENT): string(commitment)})
	}

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_IS_BLOCKHASH_VALID, params)

	return
}
