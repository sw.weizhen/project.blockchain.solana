package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
)

func (ref *Caller) GetSlot(ctx context.Context) (out uint64, err error) {

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_GET_SLOT, nil)

	return
}
