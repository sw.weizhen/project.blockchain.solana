package rpc

import (
	"context"
	"net/http"
)

// https://pkg.go.dev/github.com/DivPro/tg/v2/example/clients/example/jsonrpc
type ClientRPCer interface {
	// Call(ctx context.Context, method string, params ...interface{}) (*JSONRPCResponse, error)
	// CallBatch(ctx context.Context, requests []*JSONRPCRequest) ([]*JSONRPCResponse, error)
	// CallBatchRaw(ctx context.Context, requests []*JSONRPCRequest) ([]*JSONRPCResponse, error)
	// CallFor(ctx context.Context, out interface{}, method string, params ...interface{}) error
	// CallRaw(ctx context.Context, request *JSONRPCRequest) (*JSONRPCResponse, error)
	// HTTPClienter

	CallRef(ctx context.Context, out interface{}, method string, params []interface{}) error
	Close() error
}

// https://forum.golangbridge.org/t/when-should-i-use-client-closeidleconnections/19254
type ClientHTTPer interface {
	CloseIdleConnections()
	Do(*http.Request) (*http.Response, error)

	// Get(string) (*http.Response, error)
	// Head(string) (*http.Response, error)
	// Post(string, string, io.Reader) (*http.Response, error)
	// PostForm(string, url.Values) (*http.Response, error)
}
