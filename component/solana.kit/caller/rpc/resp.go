package rpc

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
)

type RPCResponse struct {
	ID      any             `json:"id"`
	JSONRPC string          `json:"jsonrpc"`
	Result  json.RawMessage `json:"result"`
	Err     *ErrRPC         `json:"error"`
}

func (ref *RPCResponse) Resp(resp interface{}) error {

	if ref == nil {
		return errors.New("nil response")
	}

	rftVal := reflect.ValueOf(resp)

	if rftVal.Kind() != reflect.Ptr {
		return fmt.Errorf("not pointer, response: %s", reflect.TypeOf(resp))
	}

	if ref.Result == nil {
		ref.Result = []byte(`null`)
	}

	return json.Unmarshal(ref.Result, resp)
}
