package rpc

const (
	cJSONVer = "2.0"
)

const (
	cPOST             = "POST"
	cACCEPT           = "Accept"
	cCONTENT_TYPE     = "Content-Type"
	cAPPLICATION_JSON = "application/json"
)
