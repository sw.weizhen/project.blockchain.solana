package rpc

type RPCRequest struct {
	ID      string      `json:"id"`
	Method  string      `json:"method"`
	JSONRPC string      `json:"jsonrpc"`
	Param   interface{} `json:"params"`
}
