package rpc

type ErrHTTP struct {
	Code    int
	httpErr error
}

func (ref *ErrHTTP) Error() string {
	return ref.httpErr.Error()
}
