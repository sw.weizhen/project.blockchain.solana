package rpc

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// impl CloseIdleConnections()
// impl Do(*http.Request) (*http.Response, error)
// impl CallRef
type rpcClient struct {
	httpClient ClientHTTPer

	endpoint string
	opts     map[string]string // for head set
}

type RPCClientOpts struct {
	HTTPClient ClientHTTPer
	Opts       map[string]string
}

func (ref *rpcClient) nReq(ctx context.Context, req interface{}) (*http.Request, error) {

	body, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	// https://pkg.go.dev/net/http
	reqt, err := http.NewRequestWithContext(ctx, cPOST, ref.endpoint, bytes.NewReader(body))
	if err != nil {
		return reqt, err
	}

	reqt.Header.Set(cCONTENT_TYPE, cAPPLICATION_JSON)
	reqt.Header.Set(cACCEPT, cAPPLICATION_JSON)

	for k, opt := range ref.opts {
		reqt.Header.Set(k, opt)
	}

	return reqt, nil
}

func (ref *rpcClient) callWithCallback(ctx context.Context, rpcRequest *RPCRequest, callback func(*http.Request, *http.Response) error) error {

	if rpcRequest != nil && len(rpcRequest.ID) == 0 {
		rpcRequest.ID = generateUUID()
	}

	req, err := ref.nReq(ctx, rpcRequest)
	if err != nil {
		if req != nil {
			return fmt.Errorf("rpc call %s on %s: %w", rpcRequest.Method, req.URL.String(), err)
		}

		return fmt.Errorf("rpc call %s: %w", rpcRequest.Method, err)
	}

	httpResp, err := ref.httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("rpc call %s on %s: %w", rpcRequest.Method, req.URL.String(), err)
	}

	defer httpResp.Body.Close()

	return callback(req, httpResp)
}

func (ref *rpcClient) call(ctx context.Context, RPCRequest *RPCRequest) (*RPCResponse, error) {

	var jsonRPCResponse *RPCResponse

	err := ref.callWithCallback(ctx, RPCRequest, func(httpRequest *http.Request, httpResponse *http.Response) error {

		decoder := json.NewDecoder(httpResponse.Body)
		decoder.DisallowUnknownFields()
		decoder.UseNumber()

		err := decoder.Decode(&jsonRPCResponse)
		if err != nil {
			if httpResponse.StatusCode >= 400 {

				return &ErrHTTP{
					Code:    httpResponse.StatusCode,
					httpErr: fmt.Errorf("rpc call %s on %s code: %d. decode response fail: %w", RPCRequest.Method, httpRequest.URL.String(), httpResponse.StatusCode, err),
				}
			}

			return fmt.Errorf("rpc call %s on %s code: %d. decode response fail: %w", RPCRequest.Method, httpRequest.URL.String(), httpResponse.StatusCode, err)
		}

		if jsonRPCResponse == nil {
			if httpResponse.StatusCode >= 400 {

				return &ErrHTTP{
					Code:    httpResponse.StatusCode,
					httpErr: fmt.Errorf("rpc call %s on %s code: %d. lost response", RPCRequest.Method, httpRequest.URL.String(), httpResponse.StatusCode),
				}
			}

			return fmt.Errorf("rpc call %s on %s code: %d. lost response", RPCRequest.Method, httpRequest.URL.String(), httpResponse.StatusCode)
		}

		return nil
	})

	if err != nil {
		return nil, err
	}

	return jsonRPCResponse, nil
}

func (ref *rpcClient) CallRef(ctx context.Context, out interface{}, method string, params []interface{}) error {

	request := &RPCRequest{
		Method:  method,
		JSONRPC: cJSONVer,
	}

	if params != nil {
		request.Param = params
	}

	rpcResponse, err := ref.call(ctx, request)
	if err != nil {
		return err
	}

	if rpcResponse.Err != nil {
		return rpcResponse.Err
	}

	return rpcResponse.Resp(out)
}

func (ref *rpcClient) Close() error {

	if ref.httpClient != nil {
		ref.httpClient.CloseIdleConnections()
	}

	return nil
}

func NewWithOpts(endpoint string, opts *RPCClientOpts) ClientRPCer {

	rpcClient := &rpcClient{
		endpoint:   endpoint,
		httpClient: &http.Client{},
		opts:       map[string]string{},
	}

	if opts == nil {
		return rpcClient
	}

	if opts.HTTPClient != nil {
		rpcClient.httpClient = opts.HTTPClient
	}

	if opts.Opts != nil {
		for k, opt := range opts.Opts {
			rpcClient.opts[k] = opt
		}
	}

	return rpcClient
}

func New(endpoint string) ClientRPCer {

	return NewWithOpts(endpoint, nil)
}
