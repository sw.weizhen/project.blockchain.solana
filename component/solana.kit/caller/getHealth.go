package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
)

func (ref *Caller) GetHealth(ctx context.Context) (out string, err error) {

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_GET_HEALTH, nil)

	return
}
