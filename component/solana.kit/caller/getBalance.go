package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
	"gitlab.com/sw.weizhen/solana.kit/types"
)

func (ref *Caller) GetBalance(ctx context.Context, pubKeyBase58 string, commitment namespace.Commitment) (out *types.Balance, err error) {

	params := []interface{}{pubKeyBase58}
	if len(commitment) != 0 {
		params = append(params, map[string]interface{}{string(namespace.COMMITMENT): string(commitment)})
	}

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_GET_BALANCE, params)

	return
}
