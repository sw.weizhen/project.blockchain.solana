package caller

import (
	"context"

	"gitlab.com/sw.weizhen/solana.kit/namespace"
)

func (ref *Caller) GetBlockHeight(ctx context.Context, commitment namespace.Commitment) (out uint64, err error) {

	params := []interface{}{}
	if len(commitment) != 0 {
		params = append(params, map[string]interface{}{string(namespace.COMMITMENT): string(commitment)})
	}

	err = ref.rpcClient.CallRef(ctx, &out, namespace.METHOD_GET_BLOCK_HEIGHT, params)

	return
}
