package caller

import (
	"context"
)

type RPCClienter interface {
	CallRef(ctx context.Context, out interface{}, method string, params []interface{}) error
}
