package namespace

type Encoding string

const (
	ENCODING              Encoding = "encoding"
	ENCODING_JSON         Encoding = "json"
	ENCODING_JSON_PARSED  Encoding = "jsonParsed"
	ENCODING_BASE_58      Encoding = "base58"
	ENCODING_BASE_64      Encoding = "base64"
	ENCODING_BASE_64_ZSTD Encoding = "base64+zstd"
)
