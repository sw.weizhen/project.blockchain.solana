package namespace

const (
	TxTypeDeposit     = 1
	TxTypeWithdraw    = 2
	TxTypeSelfBalance = 3
)

const (
	CryptoType = "SOL"
	ChainType  = "SOL"
)

const (
	TxStatusWaitConfirm = 0
	TxStatusSuccess     = 1
	TxStatusFail        = 2
	TxStatusRetry       = 3
)

const (
	TxNotifyStatusNotYetProcessed = 0
	TxNotifyStatusWaitNotify      = 1
	TxNotifyStatusSuccess         = 2
	TxNotifyStatusFail            = 3
	TxNotifyStatusNotNeedTodo     = 4
)
