package namespace

type Rewards string

const (
	REWARDS = "rewards"
)

type RewardType string

const (
	REWARD_TYPE         RewardType = "rewardType"
	REWARD_TYPE_FEE     RewardType = "fee"
	REWARD_TYPE_RENT    RewardType = "rent"
	REWARD_TYPE_VOTING  RewardType = "voting"
	REWARD_TYPE_STAKING RewardType = "staking"
)
