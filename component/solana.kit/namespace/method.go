package namespace

const (
	METHOD_GET_BALANCE           = "getBalance"
	METHOD_GET_BLOCK_HEIGHT      = "getBlockHeight"
	METHOD_GET_BLOCKS            = "getBlocks"
	METHOD_GET_BLOCK             = "getBlock"
	METHOD_GET_HEALTH            = "getHealth"
	METHOD_GET_LATEST_BLOCK_HASH = "getLatestBlockhash"
	METHOD_GET_SLOT              = "getSlot"
	METHOD_GET_VERSION           = "getVersion"
	METHOD_IS_BLOCKHASH_VALID    = "isBlockhashValid"
)
