package namespace

type TransactionDetails string

const (
	TRANSACTION_DETAILS            TransactionDetails = "transactionDetails"
	TRANSACTION_DETAILS_FULL       TransactionDetails = "full"
	TRANSACTION_DETAILS_ACCOUNTS   TransactionDetails = "accounts"
	TRANSACTION_DETAILS_SIGNATURES TransactionDetails = "signatures"
	TRANSACTION_DETAILS_NONE       TransactionDetails = "none"
)
