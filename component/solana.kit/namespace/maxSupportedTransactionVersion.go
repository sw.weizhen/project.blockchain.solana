package namespace

type MaxSupportedTransactionVersion string

const (
	MAX_SUPPORTED_TRANSACTION_VERSION MaxSupportedTransactionVersion = "maxSupportedTransactionVersion"
)
