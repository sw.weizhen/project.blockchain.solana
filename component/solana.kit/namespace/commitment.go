package namespace

type Commitment string

const (
	COMMITMENT           Commitment = "commitment"
	COMMITMENT_FINALIZED Commitment = "finalized"
	COMMITMENT_CONFIRMED Commitment = "confirmed"
	COMMITMENT_PROCESSED Commitment = "processed"
)
