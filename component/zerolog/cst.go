package logger

const (
	cst_LOG_PATH  = "log"
	cst_LOG_DEGUG = "debug"
	cst_LOG_INFO  = "info"
	cst_LOG_WARN  = "warn"
	cst_LOG_ERROR = "error"
)

const (
	cst_FILE_NAME = "./log/solana-%v.log"
)
