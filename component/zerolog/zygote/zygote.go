package main

import (
	"fmt"
	"log"
	"time"

	fr "project.blockchain.solana/component/zerolog"
)

func main() {
	fmt.Println("main::main() -> entry")

	err := fr.PrepareLog("debug", 1, 2)
	if err != nil {
		log.Fatal(err.Error())
	}

	count := 0
	for {
		fr.L.Info().Msgf("count: %d", count)
		count++

		<-time.After(time.Millisecond * 5)
	}
}
