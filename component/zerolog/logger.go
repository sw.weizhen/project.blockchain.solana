package logger

import (
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
	frtt "gitlab.com/sw.weizhen/util.file.rotation"
)

var L zerolog.Logger

func PrepareLog(level string, maxSize, keepDays int) error {
	fmt.Println("keepDays", keepDays)
	switch level {
	case cst_LOG_DEGUG:
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case cst_LOG_INFO:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case cst_LOG_WARN:
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case cst_LOG_ERROR:
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)

	default:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	rttFile, err := frtt.New(
		cst_FILE_NAME,
		frtt.MaxAge(time.Hour*24*time.Duration(keepDays)),
		frtt.RotationSize(1024*1024*int64(maxSize)),
		frtt.RotationTime(time.Hour*24),
	)

	if err != nil {
		return err
	}

	consoleWriter := zerolog.ConsoleWriter{
		Out:        os.Stdout,
		TimeFormat: time.DateTime,
		NoColor:    true,
	}

	L = zerolog.New(consoleWriter).Output(zerolog.MultiLevelWriter(consoleWriter, rttFile)).With().Timestamp().Logger()

	return nil
}
