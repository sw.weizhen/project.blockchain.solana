package utils

import (
	"strings"
)

func StrIsEmpty(str string) bool {

	str = strings.TrimSpace(str)

	return len(str) == 0
}
