package utils

func PtrNew[T any](obj T) *T {
	return &obj
}
