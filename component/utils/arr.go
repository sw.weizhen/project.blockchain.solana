package utils

import (
	"errors"
)

// Deprecated: abandoned, unsafe operation
// less func(x, y Generics) bool {return x.comparable < y.comparable}
func ArrPDQSort[T any](data []T, head, tail int, less func(x, y T) bool) {

	const insFactor = 16

	for tail-head > insFactor {

		if tail-head <= 1 {
			return

		} else if tail-head == 2 {

			if less(data[tail-1], data[head]) {
				data[head], data[tail-1] = data[tail-1], data[head]
			}

			return
		}

		md := fnMid(data, head, head+(tail-head)/2, tail-1, less)

		data[head], data[md] = data[md], data[head]

		pivot := fnPartition(data, head, tail, less)

		if pivot-head < tail-pivot {
			ArrPDQSort(data, head, pivot, less)
			head = pivot + 1

		} else {
			ArrPDQSort(data, pivot+1, tail, less)
			tail = pivot

		}
	}

	fnInsSort(data, head, tail, less)
}

// Deprecated: abandoned, unsafe operation
func ArrQSort(data []string, head int, tail int) {

	if head < tail {

		pivot := func(data []string, head int, tail int) (idx int) {

			val := data[tail]
			idx = head - 1

			for i := head; i < (tail); i++ {
				if data[i] <= val {
					idx++

					data[idx], data[i] = data[i], data[idx]
				}
			}

			idx++

			data[idx], data[tail] = val, data[idx]

			return idx

		}(data, head, tail)

		ArrQSort(data, head, pivot-1)
		ArrQSort(data, pivot+1, tail)
	}
}

func ArrMSort(data []string) []string {

	if len(data) <= 1 {
		return data
	}

	m := len(data) / 2
	l := ArrMSort(data[:m])
	r := ArrMSort(data[m:])

	return func(d1, d2 []string) []string {

		l1 := len(d1)
		l2 := len(d2)

		merged := make([]string, l1+l2)

		iD1 := 0
		iD2 := 0
		idx := 0

		for iD1 < l1 && iD2 < l2 {

			if d1[iD1] <= d2[iD2] {
				merged[idx] = d1[iD1]
				iD1++

			} else {
				merged[idx] = d2[iD2]
				iD2++
			}

			idx++
		}

		for iD1 < l1 {
			merged[idx] = d1[iD1]

			iD1++
			idx++
		}

		for iD2 < l2 {
			merged[idx] = d2[iD2]

			iD2++
			idx++
		}

		return merged

	}(l, r)
}

func ArrFilReplica(data []string) []string {

	if len(data) <= 1 {
		return data
	}

	idx := 0

	for _, elem := range data {
		if data[idx] != elem {
			idx++

			data[idx] = elem
		}
	}

	res := make([]string, idx+1)

	copy(res, data[:idx+1])

	return res
}

func ArrSearch(goal string, data []string) int {

	if len(data) == 0 {
		return -1
	}

	head := 0
	tail := len(data) - 1

	for head <= tail {

		idx := head + (tail-head)/2

		if data[idx] == goal {
			return idx
		}

		if data[idx] > goal {
			tail = idx - 1

		} else {

			head = idx + 1
		}
	}

	return -1
}

func ArrSearchWithLimit(goal string, data []string, limit int) (int, error) {

	if len(data) == 0 {
		return -1, nil
	}

	if limit < 10 {
		limit = 10
	}

	cnt := 0
	head := 0
	tail := len(data) - 1

	for head <= tail {

		if cnt >= limit {
			return -1, errors.New("loop limit exceeded")
		}

		idx := head + (tail-head)/2

		if data[idx] == goal {
			return idx, nil
		}

		if data[idx] > goal {
			tail = idx - 1

		} else {

			head = idx + 1
		}

		cnt++
	}

	return -1, nil
}

func ArrSearchRNG(goal string, data []string) (int, int) {

	fnHead := func() int {
		head := 0
		tail := len(data) - 1

		for head < tail {
			mid := head + (tail-head)/2

			if data[mid] < goal {
				head = mid + 1
			} else {
				tail = mid
			}
		}

		if head < len(data) && data[head] == goal {
			return head
		}

		return -1
	}

	fnTail := func() int {
		head := 0
		tail := len(data) - 1

		for head < tail {
			mid := (head + tail + 1) / 2

			if data[mid] > goal {
				tail = mid - 1
			} else {
				head = mid
			}
		}

		if head < len(data) && data[head] == goal {
			return head
		}

		return -1
	}

	head := fnHead()

	if head == -1 {
		return -1, -1
	}

	return head, fnTail()
}

// on large slice, P:pollution
func ArrTPBatch[T any](batch int, data []T) [][]T {

	if batch == 0 {
		return [][]T{}
	}

	res := make([][]T, 0, (len(data)+batch-1)/batch)

	for batch < len(data) {

		res = append(res, data[0:batch:batch])
		data = data[batch:]
	}

	return append(res, data)
}

func ArrTBatch[T any](batch int, data []T) [][]T {

	if batch == 0 {
		return [][]T{}
	}

	res := make([][]T, 0, (len(data)+batch-1)/batch)

	lD := len(data)
	for i := 0; i < lD; i += batch {

		idx := i + batch
		if idx > lD {
			idx = lD
		}

		tmp := make([]T, len(data[i:idx]))
		copy(tmp, data[i:idx])

		res = append(res, tmp)
	}

	return res
}
