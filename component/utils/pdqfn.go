package utils

func fnMid[T any](data []T, x, y, z int, less func(x, y T) bool) int {

	if less(data[y], data[x]) {
		x, y = y, x
	}

	if less(data[z], data[x]) {
		return x
	}

	if less(data[z], data[y]) {
		return z
	}

	return y
}

func fnPartition[T any](data []T, head, tail int, less func(x, y T) bool) int {

	pivodIdx := head
	pivodVal := data[head]

	head++

	for {
		for head < tail && less(data[head], pivodVal) {
			head++
		}

		for head < tail && !less(data[tail-1], pivodVal) {
			tail--
		}

		if head >= tail {
			break
		}

		data[head], data[tail-1] = data[tail-1], data[head]

		head++
		tail--
	}

	data[pivodIdx], data[tail-1] = data[tail-1], data[pivodIdx]

	return tail - 1
}

func fnInsSort[T any](data []T, head, tail int, less func(x, y T) bool) {

	for i := head + 1; i < tail; i++ {
		for j := i; j > head && less(data[j], data[j-1]); j-- {
			data[j], data[j-1] = data[j-1], data[j]
		}
	}
}
