package utils

import (
	"strconv"
	"time"
)

func TmrNow() int64 {
	return time.Now().UnixMilli()
}

func TmrCost(t int64) int64 {
	return time.Now().UnixMilli() - t
}

func NumHexToDec(hexStr string) (int64, error) {

	dec, err := strconv.ParseInt(hexStr, 16, 64)
	if err != nil {
		return 0, err
	}

	return dec, nil
}
