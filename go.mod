module project.blockchain.solana

go 1.22.3

require (
	filippo.io/edwards25519 v1.1.0
	github.com/google/uuid v1.6.0
	github.com/klauspost/compress v1.17.9
	github.com/mr-tron/base58 v1.2.0
	github.com/rs/zerolog v1.33.0
	github.com/shopspring/decimal v1.4.0
	gitlab.com/sw.weizhen/db.mysql v0.0.7
	gitlab.com/sw.weizhen/solana.kit v0.0.2
	gitlab.com/sw.weizhen/util.file.rotation v0.0.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/go-sql-driver/mysql v1.8.1 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.12.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
