package main

import (
	"project.blockchain.solana/server"
)

func main() {

	server.Boot().Run()

}
