FROM golang:1.22.3 AS build

# RUN mkdir /hsress.com/solana
RUN mkdir -p /app

# WORKDIR /hsress.com/solana
WORKDIR /app

COPY go.mod .

RUN go mod tidy
COPY . .

RUN CGO_ENABLED=0
RUN GOOS=linux
RUN GOARCH=amd64
RUN go build -o solana_block 

ENTRYPOINT ["./solana_block"]
